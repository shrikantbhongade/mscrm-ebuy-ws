﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServiceMSCRM9._0
{
    public class DateTimeHelper
    {
        //public static int getTimeOffset(string timeZoneStringInput, DateTime inputDateTime)
        //{
        //    String timeZoneString = getCompleteTimeZoneMap().get(timeZoneStringInput);
        //    if (timeZoneString == null || timeZoneString == string.Empty)
        //    {
        //        timeZoneStringInput = timeZoneStringInput.toUpperCase();
        //        timeZoneString = getShortTimeZoneMap().get(timeZoneStringInput);
        //    }
        //    if (timeZoneString != null && timeZoneString != string.Empty)
        //    {
        //        TimeZone tz = TimeZone.CurrentTimeZone(timeZoneString);
        //        int millisecondsDiff = tz.GetUtcOffset(inputDateTime);
        //        return millisecondsDiff;
        //    }
        //    return 0;
        //}
        public static string getShortTimeZoneMap(string shortTime)
        {
            try
            {
                var data = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("LIT", "Pacific/Kiritimati"),
                    new KeyValuePair<string, string>("PIT", "Pacific/Enderbury"),
                    new KeyValuePair<string, string>("TT", "Pacific/Tongatapu"),
                    new KeyValuePair<string, string>("NZST", "Pacific/Auckland"),
                    new KeyValuePair<string, string>("FT", "Pacific/Fiji"),
                    new KeyValuePair<string, string>("PKT","Asia/Kamchatka"),
                    new KeyValuePair<string, string>("NT","Pacific/Norfolk"),
                    new KeyValuePair<string, string>("LHST","Australia/Lord_Howe"),
                    new KeyValuePair<string, string>("SIT","Pacific/Guadalcanal"),
                    new KeyValuePair<string, string>("ACST","Australia/Adelaide"),
                    new KeyValuePair<string, string>("AEST","Australia/Sydney"),
                    new KeyValuePair<string, string>("AEST","Australia/Brisbane"),
                    new KeyValuePair<string, string>("ACST","Australia/Darwin"),
                    new KeyValuePair<string, string>("KST","Asia/Seoul"),
                    new KeyValuePair<string, string>("JST","Asia/Tokyo"),
                    new KeyValuePair<string, string>("HKT","Asia/Hong_Kong"),
                    new KeyValuePair<string, string>("MT","Asia/Kuala_Lumpur"),
                    new KeyValuePair<string, string>("PT","Asia/Manila"),
                    new KeyValuePair<string, string>("ST","Asia/Singapore"),
                    new KeyValuePair<string, string>("AWST","Australia/Perth"),
                    new KeyValuePair<string, string>("IT","Asia/Bangkok"),
                    new KeyValuePair<string, string>("IT","Asia/Ho_Chi_Minh"),
                    new KeyValuePair<string, string>("WIT","Asia/Jakarta"),
                    new KeyValuePair<string, string>("MT","Asia/Rangoon"),
                    new KeyValuePair<string, string>("BT","Asia/Dhaka"),
                    new KeyValuePair<string, string>("NT","Asia/Kathmandu"),
                    new KeyValuePair<string, string>("IST","Asia/Colombo"),
                    new KeyValuePair<string, string>("IST","Asia/Kolkata"),
                    new KeyValuePair<string, string>("PT","Asia/Karachi"),
                    new KeyValuePair<string, string>("UT","Asia/Tashkent"),
                    new KeyValuePair<string, string>("YT","Asia/Yekaterinburg"),
                    new KeyValuePair<string, string>("AT","Asia/Kabul"),
                    new KeyValuePair<string, string>("AST","Asia/Baku"),
                    new KeyValuePair<string, string>("GST","Asia/Dubai"),
                    new KeyValuePair<string, string>("GT","Asia/Tbilisi"),
                    new KeyValuePair<string, string>("AT","Asia/Yerevan"),
                    new KeyValuePair<string, string>("IDT","Asia/Tehran"),
                    new KeyValuePair<string, string>("EAT","Africa/Nairobi"),
                    new KeyValuePair<string, string>("AST","Asia/Baghdad"),
                    new KeyValuePair<string, string>("AST","Asia/Kuwait"),
                    new KeyValuePair<string, string>("AST","Asia/Riyadh"),
                    new KeyValuePair<string, string>("MST","Europe/Minsk"),
                    new KeyValuePair<string, string>("MST","Europe/Moscow"),
                    new KeyValuePair<string, string>("EEST","Africa/Cairo"),
                    new KeyValuePair<string, string>("EEST","Asia/Beirut"),
                    new KeyValuePair<string, string>("IDT","Asia/Jerusalem"),
                    new KeyValuePair<string, string>("EEST","Europe/Athens"),
                    new KeyValuePair<string, string>("EEST","Europe/Bucharest"),
                    new KeyValuePair<string, string>("EEST","Europe/Helsinki"),
                    new KeyValuePair<string, string>("EEST","Europe/Istanbul"),
                    new KeyValuePair<string, string>("SAST","Africa/Johannesburg"),
                    new KeyValuePair<string, string>("CEST","Europe/Amsterdam"),
                    new KeyValuePair<string, string>("CEST","Europe/Berlin"),
                    new KeyValuePair<string, string>("CEST","Europe/Brussels"),
                    new KeyValuePair<string, string>("CEST","Europe/Paris"),
                    new KeyValuePair<string, string>("CEST","Europe/Prague"),
                    new KeyValuePair<string, string>("CEST","Europe/Rome"),
                    new KeyValuePair<string, string>("WEST","Europe/Lisbon"),
                    new KeyValuePair<string, string>("CET","Africa/Algiers"),
                    new KeyValuePair<string, string>("BST","Europe/London"),
                    new KeyValuePair<string, string>("CVT","Atlantic/Cape_Verde"),
                    new KeyValuePair<string, string>("WET","Africa/Casablanca"),
                    new KeyValuePair<string, string>("IST","Europe/Dublin"),
                    new KeyValuePair<string, string>("GMT","GMT"),
                    new KeyValuePair<string, string>("EGST","America/Scoresbysund"),
                    new KeyValuePair<string, string>("AST","Atlantic/Azores"),
                    new KeyValuePair<string, string>("SGST","Atlantic/South_Georgia"),
                    new KeyValuePair<string, string>("NDT","America/St_Johns"),
                    new KeyValuePair<string, string>("BST","America/Sao_Paulo"),
                    new KeyValuePair<string, string>("AT","America/Argentina/Buenos_Aires"),
                    new KeyValuePair<string, string>("ADT","America/Halifax"),
                    new KeyValuePair<string, string>("AST","America/Puerto_Rico"),
                    new KeyValuePair<string, string>("ADT","Atlantic/Bermuda"),
                    new KeyValuePair<string, string>("VT","America/Caracas"),
                    new KeyValuePair<string, string>("EDT","America/New_York"),
                    new KeyValuePair<string, string>("ET","America/New_York"),
                    new KeyValuePair<string, string>("CT","America/Chicago"),
                    new KeyValuePair<string, string>("PT","America/Los_Angeles"),
                    new KeyValuePair<string, string>("EST","America/New_York"),
                    new KeyValuePair<string, string>("CDT","America/Chicago"),
                    new KeyValuePair<string, string>("CST","America/Chicago"),
                    new KeyValuePair<string, string>("MDT","America/Denver"),
                    new KeyValuePair<string, string>("MST","America/Denver"),
                    new KeyValuePair<string, string>("PDT","America/Los_Angeles"),
                    new KeyValuePair<string, string>("PST","America/Los_Angeles"),
                    new KeyValuePair<string, string>("ADT","America/Anchorage"),
                    new KeyValuePair<string, string>("GT","Pacific/Gambier"),
                    new KeyValuePair<string, string>("HAST","America/Adak"),
                    new KeyValuePair<string, string>("MT","America/Denver"),
                    new KeyValuePair<string, string>("HAST","Pacific/Honolulu"),
                    new KeyValuePair<string, string>("NT","Pacific/Niue"),
                    new KeyValuePair<string, string>("SST","Pacific/Pago_Pago")

                };
                var output = data.Select(a => a.Key.Equals(shortTime));
                string value = data.First(kvp => kvp.Key == shortTime).Value;
                return value;
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("getShortTimeZoneMap " + ex.Message.ToString());
            }
            return string.Empty;
        }
    }
}