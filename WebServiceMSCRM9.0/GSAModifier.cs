﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Web;

namespace WebServiceMSCRM9._0
{
    public class GSAModifier
    {
        public static string parseModification(string inputBody, string modificationName, string taskOrderId)
        {
            try
            {
                if (!string.IsNullOrEmpty(inputBody))
                {
                    string inputWithoutHtml = inputBody.StripHtmlTags().Trim();
                    string inputeData = inputWithoutHtml.SubstringAfter("RFQ ID:");
                    //return "inputeData:- " + inputeData;
                    modificationName = modificationName.Trim();
                    if (taskOrderId != "undefined")
                    {

                        if (!string.IsNullOrEmpty(taskOrderId.Trim()))
                        {
                            //if (taskOrderId != null && taskOrderId != string.Empty && modificationName != null && modificationName != string.Empty)
                            if (!string.IsNullOrEmpty(modificationName))
                            {
                                IOrganizationService organizationService = null;
                                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                                string password = ConfigurationManager.AppSettings["Password"].ToString();
                                ClientCredentials clientCredentials = new ClientCredentials();
                                clientCredentials.UserName.UserName = userName;
                                clientCredentials.UserName.Password = password;
                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                                 null, clientCredentials, null);
                                if (organizationService != null)
                                {
                                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                                    if (userid != Guid.Empty)
                                    {
                                        QueryExpression queryTaskOrdersCheck = new QueryExpression();
                                        queryTaskOrdersCheck.EntityName = "fedcap_taskorder";
                                        queryTaskOrdersCheck.ColumnSet = new ColumnSet();
                                        queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_name");
                                        queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskordernumber");
                                        queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskorderid");
                                        queryTaskOrdersCheck.Criteria = new FilterExpression();
                                        queryTaskOrdersCheck.Criteria.AddCondition("fedcap_taskorderid", ConditionOperator.Equal, new Guid(taskOrderId));
                                        EntityCollection oldTaskOrdersList = organizationService.RetrieveMultiple(queryTaskOrdersCheck);
                                        if (oldTaskOrdersList.Entities.Count > 0)
                                        {
                                            var taskOrder = oldTaskOrdersList.Entities[0];
                                            string taskOrderNumber = taskOrder.Attributes["fedcap_taskordernumber"].ToString();

                                            QueryExpression queryContractMod = new QueryExpression();
                                            queryContractMod.EntityName = "fedcap_contractmods";
                                            queryContractMod.ColumnSet = new ColumnSet();
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_name");
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_taskorder");
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_contractmodsid");
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_modificationname");
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_modification");
                                            queryContractMod.ColumnSet.Columns.Add("fedcap_contractmodstype");
                                            //queryContractMod.PageInfo.Count = 1;
                                            //queryContractMod.PageInfo.PageNumber = 1;
                                            //queryContractMod.TopCount = 1;
                                            queryContractMod.Criteria = new FilterExpression();
                                            queryContractMod.Criteria.AddCondition("fedcap_modificationname", ConditionOperator.Equal, modificationName);
                                            queryContractMod.Criteria.AddCondition("fedcap_taskorder", ConditionOperator.Equal, new Guid(taskOrderId));
                                            EntityCollection contractModeList = organizationService.RetrieveMultiple(queryContractMod);
                                            //var contractModRecord = new Entity("fedcap_contractmods");
                                            var contractMod = new Entity("fedcap_contractmods");
                                            if (contractModeList != null && contractModeList.Entities.Count > 0)
                                            {
                                                var contractModRecord = contractModeList.Entities[0];
                                                var fedcap_modification = contractModRecord.Attributes["fedcap_modification"].ToString();
                                                if (!inputeData.Trim().Contains("Description of modification made at :"))
                                                {
                                                    contractMod.Attributes["fedcap_contractmodstype"] = new OptionSetValue(0);
                                                    contractMod.Attributes["fedcap_modificationname"] = modificationName;
                                                    contractMod.Attributes["fedcap_modification"] = fedcap_modification + inputeData;
                                                    contractMod.Attributes["fedcap_taskorder"] = new EntityReference(id: new Guid(taskOrderId), logicalName: "fedcap_taskorder");

                                                    contractMod.Id = contractModRecord.Id;
                                                    organizationService.Update(contractMod);
                                                    //update contractMod;
                                                }
                                            }
                                            else
                                            {
                                                contractMod.Attributes["fedcap_contractmodstype"] = new OptionSetValue(0);
                                                contractMod.Attributes["fedcap_modificationname"] = modificationName;
                                                contractMod.Attributes["fedcap_modification"] = inputeData;
                                                contractMod.Attributes["fedcap_taskorder"] = new EntityReference(id: new Guid(taskOrderId), logicalName: "fedcap_taskorder");
                                                organizationService.Create(contractMod);
                                            }
                                            string retrnURLString = string.Empty;
                                            if (inputeData.Contains("The following Q&A document(s) are attached to this RFQ:"))
                                            {
                                                string[] rows = inputBody.Split('\n');
                                                retrnURLString = "rows.Length:- " + rows.Length;
                                                foreach (var row in rows)
                                                {
                                                    if (row.Contains("/advantage/ebuy/view_doc.do"))
                                                    {
                                                        string documentName = row.StripHtmlTags().Trim();
                                                        string tempUrl = row.SubStringBetween("href=", ">").Trim();
                                                        tempUrl = tempUrl.Replace("\'", string.Empty);
                                                        tempUrl = tempUrl.Replace("\"", string.Empty);

                                                        string fileName = tempUrl.SubStringBetween("filename=", "&path=");
                                                        int index3 = fileName.LastIndexOf('.');
                                                        string fileExtension = +'.' + fileName.SubstringAfter(".");

                                                        retrnURLString += "https://www.ebuy.gsa.gov" + tempUrl + "SPLITDATA" + documentName + fileExtension + "SPLITDATA" + taskOrderId + "SPLITURL";
                                                    }
                                                    else
                                                    {
                                                        retrnURLString += "advantage/ebuy/view_doc.do";
                                                    }
                                                }
                                                return retrnURLString;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("parseModification - taskOrderId - " + taskOrderId + ";" + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return string.Empty;
        }
    }
}