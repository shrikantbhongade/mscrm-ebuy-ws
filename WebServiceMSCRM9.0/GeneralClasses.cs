﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
namespace WebServiceMSCRM9._0
{
    public static class GeneralClasses
    {
        public static string SubstringAfter(this string source, string value)
        {
            if (string.IsNullOrEmpty(source) || string.IsNullOrEmpty(value))
                return source;
            int index = source.IndexOf(value, StringComparison.Ordinal);
            return (index >= 0)
                ? source.Substring(index + value.Length)
                : source;
        }
        public static string SubStringBetween(this string STR, string FirstString, string LastString)
        {
            string FinalString = string.Empty;
            try
            {
                if (FirstString.Length > 0)
                {
                    int Pos1 = STR.IndexOf(FirstString) + FirstString.Length;
                    int Pos2 = STR.IndexOf(LastString);
                    FinalString = STR.Substring(Pos1, Pos2 - Pos1);
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SubStringBetween " + ex.Message.ToString());
            }
            return FinalString;
        }
        public static string StripHtmlTags(this string source)
        {
            return Regex.Replace(source, "<.*?>|&.*?;", string.Empty);
        }

        public static string FindHrefs(this string input)
        {
            Regex regex = new Regex("href\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))", RegexOptions.IgnoreCase);
            Match match;
            string link = string.Empty;
            for (match = regex.Match(input); match.Success; match = match.NextMatch())
            {
                Console.WriteLine("Found a href. Groups: ");
                foreach (Group group in match.Groups)
                {
                    //Console.WriteLine("Group value: {0}", group);
                    link = group.ToString();
                }
            }
            return link;
        }
    }
}