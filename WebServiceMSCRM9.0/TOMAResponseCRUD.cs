﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Text;
using System.Web;

namespace WebServiceMSCRM9._0
{
    public class TOMAResponseCRUD
    {
        public static string SaveTOMASurvey(string taskOrderId, string contactId, string tomaquestionnaireid)
        {
            string responseId = string.Empty;
            try
            {
                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        Guid responseGUID = new Guid();
                        var contractMod = new Entity("fedtom_tomaquestionnaireresponse");
                        contractMod.Attributes["fedtom_taskorder"] = new EntityReference(id: new Guid(taskOrderId), logicalName: "fedcap_taskorder");
                        contractMod.Attributes["fedtom_contact"] = new EntityReference(id: new Guid(contactId), logicalName: "contact");
                        contractMod.Attributes["fedtom_surveyname"] = new EntityReference(id: new Guid(tomaquestionnaireid), logicalName: "fedtom_tomaquestionnaire");
                        contractMod.Attributes["fedtom_dateresponsereceived"] = DateTime.Now;
                        responseGUID = organizationService.Create(contractMod);
                        responseId = responseGUID.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurvey - " + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return responseId;
        }
        public static string SaveTOMASurveyAnswers(string responseId, string questionId, string givenAnswer)
        {
            string answerId = string.Empty;

            try
            {
                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        Guid answerGUID = new Guid();
                        var TOMA_Answer = new Entity("fedtom_teamingrequestquestionnaireanswer");
                        TOMA_Answer.Attributes["fedtom_questions"] = new EntityReference(id: new Guid(questionId), logicalName: "fedtom_tomaquestions");
                        TOMA_Answer.Attributes["fedtom_feedback"] = new EntityReference(id: new Guid(responseId), logicalName: "fedtom_tomaquestionnaireresponse");
                        TOMA_Answer.Attributes["fedtom_givenanswer"] = givenAnswer;
                        answerGUID = organizationService.Create(TOMA_Answer);
                        answerId = answerGUID.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers - " + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return answerId;
        }
        public static TokenClass TokenGenerate()
        {
            TokenClass tocken = new TokenClass();
            try
            {
                string grant_type = ConfigurationManager.AppSettings["grant_type"].ToString();
                string resource = ConfigurationManager.AppSettings["resource"].ToString();
                string client_id = ConfigurationManager.AppSettings["client_id"].ToString();
                string client_secret = ConfigurationManager.AppSettings["client_secret"].ToString();
                string tenant_id = ConfigurationManager.AppSettings["tenant_id"].ToString();

                using (WebClient wc = new WebClient())
                {
                    wc.Proxy = null;

                    NameValueCollection col = new NameValueCollection();
                    col.Add("grant_type", grant_type);
                    col.Add("resource", resource);
                    col.Add("client_id", client_id);
                    col.Add("client_secret", client_secret);

                    String response = "";
                    try
                    {
                        byte[] data = wc.UploadValues("https://login.microsoftonline.com/" + tenant_id + "/oauth2/token", "POST", col);
                        response = Encoding.UTF8.GetString(data);
                    }
                    catch (WebException e)
                    {
                        response = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                    }
                    tocken = JsonConvert.DeserializeObject<TokenClass>(response);
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("TokenGenerate - " + ex.Message.ToString());
            }
            return tocken;
        }
        public static string SaveTOMASurvey_Azure(string taskOrderId, string contactId, string tomaquestionnaireid)
        {
            string responseId = string.Empty;
            DateTime dtToday = DateTime.Now;
            string dtTodayStr = dtToday.Year.ToString() + "-" + dtToday.Month.ToString() + "-" + dtToday.Day;
            try
            {
                TokenClass tocken = TokenGenerate(); ;
                string resource = ConfigurationManager.AppSettings["resource"].ToString();
                string webAddr = resource + "api/data/v9.0/fedtom_tomaquestionnaireresponses";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + tocken.access_token.ToString());
                httpWebRequest.Method = "POST";
               
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    JObject jsonRecord = new JObject{
                        { "fedtom_TaskOrder@odata.bind", "/fedtom_tomaquestionnaireresponses("+taskOrderId+")" },//lookup
                        { "fedtom_Contact@odata.bind", "/fedtom_tomaquestionnaireresponses("+contactId+")" },//lookup
                        { "fedtom_SurveyName@odata.bind", "/fedtom_tomaquestionnaireresponses("+tomaquestionnaireid+")" },//lookup
                        { "fedtom_dateresponsereceived",  dtTodayStr},//date
                        };
                    streamWriter.Write(jsonRecord);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    responseText = httpResponse.Headers.GetValues("OData-EntityId").FirstOrDefault();
                    string splitResponse = responseText.Split('(')[1].Substring(0, 36);
                    responseId = splitResponse;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurvey - " + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return responseId;
        }
        public static string SaveTOMASurveyAnswers_Azure(string responseId, string questionId, string givenAnswer)
        {
            string answerId = string.Empty;
            try
            {
                TokenClass tocken = TokenGenerate(); ;
                string resource = ConfigurationManager.AppSettings["resource"].ToString();
                string webAddr = resource + "api/data/v9.0/fedtom_teamingrequestquestionnaireanswers";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Headers.Add("Authorization", "Bearer " + tocken.access_token.ToString());
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    JObject jsonRecord = new JObject{
                        { "fedtom_Feedback@odata.bind", "/fedtom_teamingrequestquestionnaireanswers("+responseId+")" },//lookup
                        { "fedtom_Questions@odata.bind", "/fedtom_teamingrequestquestionnaireanswers("+questionId+")" },//lookup
                        { "fedtom_givenanswer",  givenAnswer },//string
                        };
                    streamWriter.Write(jsonRecord);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    responseText = httpResponse.Headers.GetValues("OData-EntityId").FirstOrDefault();
                    string splitResponse = responseText.Split('(')[1].Substring(0, 36);
                    answerId = splitResponse;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers - " + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return answerId;
        }
    }
    public class TokenClass
    {
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string ext_expires_in { get; set; }
        public int expires_on { get; set; }
        public string not_before { get; set; }
        public string resource { get; set; }
        public string access_token { get; set; }
    }
}