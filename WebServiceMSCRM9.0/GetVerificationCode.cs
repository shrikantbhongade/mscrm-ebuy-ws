﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Web;

namespace WebServiceMSCRM9._0
{
    public class GetVerificationCode
    {
        public static string GetEbuyVerificationCode()
        {
            string verificationCode = string.Empty;
            try
            {
                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        QueryExpression queryVerificationCode = new QueryExpression();
                        queryVerificationCode.EntityName = "fedcap_ebuyverificationcode";
                        queryVerificationCode.ColumnSet = new ColumnSet();
                        queryVerificationCode.ColumnSet.Columns.Add("fedcap_name");
                        queryVerificationCode.AddOrder("createdon", OrderType.Descending);
                        queryVerificationCode.PageInfo = new PagingInfo();
                        queryVerificationCode.PageInfo.Count = 1;
                        queryVerificationCode.PageInfo.PageNumber = 1;
                        EntityCollection list = organizationService.RetrieveMultiple(queryVerificationCode);
                        if (list.Entities.Count > 0)
                        {
                            var code = list.Entities[0];
                            verificationCode = code.Attributes["fedcap_name"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GetEbuyVerificationCode - " + ex.Message.ToString());
                return ex.Message.ToString();
            }
            return verificationCode;
        }
    }
}