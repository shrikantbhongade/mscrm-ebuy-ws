﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml.Linq;
using TimeZoneConverter;

namespace WebServiceMSCRM9._0
{
    /// <summary>
    /// Summary description for gsa
    /// </summary>
    [WebService(Namespace = "https://www.technomileconnect.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class gsa : System.Web.Services.WebService
    {
        private string rfqId = string.Empty;
        private string rFQIssueDate = string.Empty;
        private string rFQCloseDate = string.Empty;
        private string description = string.Empty;
        private string delivery = string.Empty;
        //private List<String> buyerDocumentsList;
        private string quoteId = string.Empty;
        private string reference = string.Empty;
        private string category = string.Empty;
        private string contact = string.Empty;
        private string fOBOriginTransportationCost = string.Empty;
        private string fOB = string.Empty;
        public string buyerDocumentsUrls = string.Empty;
        private string contractNumber = string.Empty;
        private string rfqTitle = string.Empty;
        private string responseUrl = string.Empty;
        //private Account acc;
        private string modificationURL = string.Empty;
        //New Variable for parsing
        private string taskOrderContractingActivity = string.Empty;
        private string individualReceivingShipment = string.Empty;
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GSAConnectAttach()
        {
            string statusTemp = string.Empty;
            string urls = string.Empty;
            string result = string.Empty;
            string taskOrderId = string.Empty;
            string fileName = string.Empty;
            string htmlBody = string.Empty;
            int len = 0;
            try
            {
                if (HttpContext.Current.Request.Headers.Get("fileName") != null)
                {
                    fileName = HttpContext.Current.Request.Headers.Get("fileName").ToString().Trim();
                }
                if (HttpContext.Current.Request.Headers.Get("taskOrderId") != null)
                {
                    taskOrderId = HttpContext.Current.Request.Headers.Get("taskOrderId").ToString();
                }
                
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    //htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                    htmlBody = Convert.ToBase64String(stream.ToArray());
                }
                byte[] inputBody = Encoding.ASCII.GetBytes(htmlBody);
                //htmlBody = Convert.ToBase64String(inputBody);
                

                if (fileName != null && taskOrderId != null)
                {
                    result = GSAAttacher.attachDocTO(htmlBody, fileName, taskOrderId);
                }
                else
                {
                    result = "Empty Data";
                }

            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GSAConnectAttach:- taskOrderId - " + taskOrderId + ";FileName:- " + fileName  + "; " + ex.Message.ToString());
                result = "Failed";
            }
            HttpContext.Current.Response.Write(result + " - fileName:-" + fileName);

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GSAConnectModification()
        {
            string statusTemp = string.Empty;
            string urls = string.Empty;
            string result = string.Empty;
            string taskOrderId = string.Empty;
            string modName = string.Empty;
            string htmlBody = string.Empty;
            int len = 0;
            try
            {
                if (HttpContext.Current.Request.Headers.Get("modName") != null)
                {
                    modName = HttpContext.Current.Request.Headers.Get("modName").ToString();
                }
                if (HttpContext.Current.Request.Headers.Get("taskOrderId") != null)
                {
                    taskOrderId = HttpContext.Current.Request.Headers.Get("taskOrderId").ToString();
                }
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                }
                if (htmlBody.Contains("RFQ1279439"))
                {
                    Helper.WriteToLogAndConsole("GSAConnectModification:- htmlBody:- " + htmlBody);
                }
                if (modName != null && taskOrderId != null && !htmlBody.Contains("Description of modification made at :"))
                {
                    result = GSAModifier.parseModification(htmlBody.ToString(), modName, taskOrderId);
                }
                else
                {
                    result = "Empty Data";
                }

            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GSAConnectModification:- taskOrderId:- " + taskOrderId + "; " + ex.Message.ToString());
                result = "Failed";
            }
            HttpContext.Current.Response.Write(result);

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void GSAConnectGET()
        {
            string urls = string.Empty;
            try
            {
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                string htmlBody = "";
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                }
                string[] stringSeparators = new string[] { "<tr>" };
                var rowByTrSplits = htmlBody.Split(stringSeparators, StringSplitOptions.None);
                string contractNumber = string.Empty;
                foreach (var rowByTr in rowByTrSplits)
                {
                    string sin = string.Empty;
                    string[] rows = rowByTr.Split('\n');
                    foreach (var row in rows)
                    {
                        var rowNew = string.Empty;
                        bool isContainSins = row.Contains("/advantage/ebuy/seller/category_description.do?");
                        if (isContainSins)
                        {
                            sin = row.StripHtmlTags().Trim();
                        }
                        bool isContains = row.Contains("/advantage/ebuy/seller/open_rfq.do");
                        bool isContains1 = row.Contains("/advantage/ebuy/seller/RFQ_cancel_reason.do");
                        if ((isContains || isContains1))
                        {
                            rowNew = row.FindHrefs();
                            rowNew = rowNew.Replace("&amp;", "&");
                            rowNew = rowNew.Replace("amp;", "");
                            urls += rowNew + "&sins=" + sin + "&contractNumber=" + contractNumber + "SPLITURL".Trim();
                        }
                        else
                        {
                            rowNew = row.StripHtmlTags().Trim();

                            if (rowNew.Contains("Contract Number:"))
                            {
                                contractNumber = rowNew.SubstringAfter("Contract Number:").Trim(); //row.substringAfterLast('Contract Number:').trim();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                urls = "Exception caught - " + ex.Message;
                Helper.WriteToLogAndConsole("GSAConnectGET:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(urls);
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void GSAConnectGETVerificationCode()
        {
            string VerficationCode = string.Empty;
                Helper.WriteToLogAndConsole("GSAConnectGETVerificationCode:- method called");
            try
            {
                VerficationCode = GetVerificationCode.GetEbuyVerificationCode();
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GSAConnectGETVerificationCode:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(VerficationCode);
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void SaveTOMASurey()
        {
            string save = string.Empty;
            string taskOrderId = string.Empty;
            string contactId = string.Empty;
            string htmlBody = string.Empty;
            string tomaquestionnaireid = string.Empty;
            Helper.WriteToLogAndConsole("SaveTOMASurey:- method called");
            try
            {
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                    //htmlBody = Convert.ToBase64String(stream.ToArray());
                }
                string[] param = htmlBody.Split('&');
                taskOrderId = param[0].Split('=')[1];
                contactId = param[1].Split('=')[1];
                tomaquestionnaireid = param[2].Split('=')[1];
                save = TOMAResponseCRUD.SaveTOMASurvey(taskOrderId, contactId, tomaquestionnaireid);
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurey:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(save);
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void SaveTOMASurveyAnswers()
        {
            string save = string.Empty;
            string responseId = string.Empty;
            string questionId = string.Empty;
            string givenAnswer = string.Empty;
            string htmlBody = string.Empty;
            Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers:- method called");
            try
            {
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                    //htmlBody = Convert.ToBase64String(stream.ToArray());
                }
                string[] param = htmlBody.Split('&');
                responseId = param[0].Split('=')[1];
                questionId = param[1].Split('=')[1];
                givenAnswer = param[2].Split('=')[1];
                //save = "responseId:-" + responseId + " questionId:-" + questionId + " givenAnswer:-" + givenAnswer;
                save = TOMAResponseCRUD.SaveTOMASurveyAnswers(responseId,questionId,givenAnswer);
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(save);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GSAConnectPOST()
        {
            string urls = string.Empty;
            string firstRow = string.Empty;
            try
            {
                string htmlBody = "";
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                }
                responseUrl = htmlBody.SubstringAfter("RESPONSEURL=").Trim();
                string newParseString = htmlBody.StripHtmlTags();// StripHtmlTags(htmlBody);
                string[] newParserStringSplit = newParseString.Split('\n');
                if (newParserStringSplit != null && newParserStringSplit.Length > 0)
                {
                    firstRow = newParserStringSplit[0];
                }
                if (firstRow.Contains("Cancellation Reason"))
                {
                    //rfqId = StripHtmlTags(SubStringBetween(htmlBody, "RFQ ID:", "\n")).Trim();// htmlBody.substringBetween('RFQ ID:', '\n').stripHtmlTags().trim();
                    rfqId = htmlBody.SubStringBetween("RFQ ID:", "\n").StripHtmlTags().Trim();
                    if (responseUrl != null && responseUrl.Length > 1)
                    {
                        contractNumber = responseUrl.SubstringAfter("contractNumber=");
                        string sin = responseUrl.SubStringBetween("sins=", "&contractNumber");// responseUrl.substringBetween('sins=', '&contractNumber');
                        createContractMods(rfqId, contractNumber, newParseString, sin);
                    }
                }
                else
                {
                    rfqId = htmlBody.SubStringBetween("RFQ ID:", "Reference #:");// htmlBody.substringBetween('RFQ ID:', 'Reference #:');
                    if (rfqId != null)
                    {
                        rfqId = rfqId.StripHtmlTags().Trim();// rfqId.stripHtmlTags().trim();
                    }

                    buyerDocumentsUrls += "";

                    rfqTitle = htmlBody.SubStringBetween("RFQ Title:", "Category:");
                    if (rfqTitle != null)
                    {
                        rfqTitle = rfqTitle.StripHtmlTags().Trim();
                    }
                    rFQIssueDate = string.Empty;
                    rFQIssueDate = htmlBody.SubStringBetween("RFQ Issue Date:", "Contact:");
                    if (rFQIssueDate != null)
                    {
                        rFQIssueDate = rFQIssueDate.StripHtmlTags().Trim();
                    }

                    rFQCloseDate = htmlBody.SubStringBetween("RFQ Close Date:", "Delivery:");
                    if (rFQCloseDate != null)
                    {
                        rFQCloseDate = rFQCloseDate.StripHtmlTags().Trim();
                        if (rFQCloseDate.Contains("Time Remaining"))
                        {
                            string[] splitDate = rFQCloseDate.Split('(');
                            rFQCloseDate = splitDate[0].Trim();
                        }
                    }

                    delivery = htmlBody.SubStringBetween("Delivery:", "Description:");
                    if (delivery != null)
                    {
                        delivery = delivery.StripHtmlTags().Trim();
                    }

                    description = htmlBody.SubStringBetween("Description:", "Buyer Documents:");
                    if (description != null)
                    {
                        description = description.StripHtmlTags().Trim();
                    }
                    quoteId = htmlBody.SubStringBetween("Quote ID", "Your Quote is good until:");
                    if (quoteId != null)
                    {
                        quoteId = quoteId.StripHtmlTags().Trim();
                    }
                    reference = htmlBody.SubStringBetween("Reference #:", "RFQ Title:");
                    if (reference != null)
                    {
                        reference = reference.StripHtmlTags().Trim();
                    }
                    category = htmlBody.SubStringBetween("Category:", "RFQ Issue Date:");
                    if (category != null)
                    {
                        category = category.StripHtmlTags().Replace("\t", string.Empty).Trim();
                    }
                    contact = htmlBody.SubStringBetween("Contact:", "RFQ Close Date:");
                    if (contact != null)
                    {
                        contact = contact.StripHtmlTags().Trim();
                    }
                    fOBOriginTransportationCost = htmlBody.SubStringBetween("Origin Transportation Cost:", "Attached Documents");
                    if (fOBOriginTransportationCost != null)
                    {
                        fOBOriginTransportationCost = fOBOriginTransportationCost.StripHtmlTags().Trim();
                    }

                    if (responseUrl != null && responseUrl.Length > 1)
                    {
                        contractNumber = responseUrl.SubStringBetween("contractNumber=", "&rfqId=");
                    }
                    //new parsing
                    try
                    {
                        taskOrderContractingActivity = newParseString.SubstringAfter("(1)");
                        if (taskOrderContractingActivity != null)
                        {
                            taskOrderContractingActivity = taskOrderContractingActivity.SubStringBetween(":", "Individual Receiving Shipment");
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.WriteToLogAndConsole("GSAConnectPOST - taskOrderContractingActivity: " + ex.Message.ToString());

                    }
                    try
                    {
                        individualReceivingShipment = htmlBody.SubStringBetween("Individual Receiving Shipment", "RESPONSEURL");
                        if (individualReceivingShipment != null)
                        {
                            individualReceivingShipment = individualReceivingShipment.StripHtmlTags().Trim();
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.WriteToLogAndConsole("GSAConnectPOST - individualReceivingShipment: " + ex.Message.ToString());
                    }
                   
                    string returnURL = UpsertGSADataInTaskOrder(htmlBody);
                    HttpContext.Current.Response.Write(returnURL);
                } 
                HttpContext.Current.Response.Write(string.Empty);
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write("Exception caught - " + ex.Message);
                Helper.WriteToLogAndConsole("Exception caught - GSAConnectPOST " +  ex.Message.ToString());

            }
        }
        [WebMethod]
        public void GetDate()
        {
            try
            {
                
                //string value = @"12/15/2009 6:32 PM -05:00";
                rFQIssueDate = "03/22/2018 04:48:22 PM EDT";
                string[] split = rFQIssueDate.Split(' ');
                string[] splitInt = split[0].Split('/');
                string[] splitSec = split[1].Split(':');
                DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));
                CultureInfo provider = CultureInfo.InvariantCulture;
                string strShortTime = (split[split.Length - 1]);

                //string strShortTime = rFQIssueDate.Substring(rFQIssueDate.Length -1, 3);
                string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                //rFQIssueDate = "04/09/2018 05:47:20 PM EDT";
                rFQIssueDate = rFQIssueDate.Remove(rFQIssueDate.Length - 3);
                string strTime = tzi.BaseUtcOffset.ToString();
                rFQIssueDate = rFQIssueDate + strTime;
                rFQIssueDate = rFQIssueDate.Remove(rFQIssueDate.Length - 3);
                string value = @rFQIssueDate;
                string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                DateTime date1;
                CultureInfo culture = new CultureInfo("en-US");
                if (DateTime.TryParseExact(value, fmt, culture, DateTimeStyles.AllowWhiteSpaces, out date1))
                {
                    DateTime dtNew = Convert.ToDateTime(value);
                    var UtcDateTime = TimeZoneInfo.ConvertTimeToUtc(date1, TimeZoneInfo.FindSystemTimeZoneById("UTC")); // 1st param is the date to convert and the second is its TimeZone
                    HttpContext.Current.Response.Write("Converted " + value + " to " + date1 + ", Kind " + date1.Kind);
                }
                else
                {
                      Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                }

                CultureInfo culture1 = new CultureInfo("en-US");

                //DateTime dateTimeObj = Convert.ToDateTime("10/22/2015 12:10:15 PM", culture);
                DateTime dateTimeObj = Convert.ToDateTime("03/15/2018 09:53:08 PM", culture);

                //DateTime date2 = DateTime.ParseExact(value, fmt, null, DateTimeStyles.AdjustToUniversal);

            }
            catch (FormatException ex)
            {
                Helper.WriteToLogAndConsole(ex.Message.ToString());
            }

        }
        public string UpsertGSADataInTaskOrder(string htmlBody)
        {
            Guid oldTaskOrdersGuidGbol = new Guid();
            string rfqId_Original = string.Empty;
            Guid newTaskOrder = new Guid();
            string returnURL = string.Empty;
            string buyerDocumentsUrlsComplete = string.Empty;
            string modificationURLComplete = string.Empty;
            string sin = string.Empty;
            string[] responseUrlSplit = responseUrl.Split('&');
            bool updateFlag = false;
            foreach (var item in responseUrlSplit)
            {
                if (item.ToString().Contains("sins="))
                {
                    sin = item.SubstringAfter("sins=");
                }
            }
            //if (responseUrl.SubStringBetween("sins=", "&contractNumber") != null)
            //{
            //    sin = responseUrl.SubStringBetween("sins=", "&contractNumber'").Trim();
            //}
            IOrganizationService organizationService = null;
            string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
            string userName = ConfigurationManager.AppSettings["Username"].ToString();
            string password = ConfigurationManager.AppSettings["Password"].ToString();
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        String[] strr = new String[] { "Redmond", "Bellevue", "Kirkland", "Seattle" };
                        //StringCollection contractIdSet = new StringCollection();
                        //List<string> contractIdSet = new List<string>();
                        QueryExpression queryContractVehicles = new QueryExpression();
                        queryContractVehicles.EntityName = "new_contractvehicles";
                        queryContractVehicles.ColumnSet = new ColumnSet();
                        queryContractVehicles.ColumnSet.Columns.Add("new_name");
                        queryContractVehicles.ColumnSet.Columns.Add("new_tm_toma__contract_number__c");
                        queryContractVehicles.ColumnSet.Columns.Add("new_tm_toma__sins__c");
                        queryContractVehicles.Criteria = new FilterExpression();
                        queryContractVehicles.Criteria.AddCondition("new_tm_toma__contract_number__c", ConditionOperator.Equal, contractNumber);
                        //queryContractVehicles.Criteria.AddCondition("new_tm_toma__sins__c", ConditionOperator.NotEqual, null);
                        EntityCollection listContractVehicles = organizationService.RetrieveMultiple(queryContractVehicles);
                        string[] contractIdSet = null;
                        if (listContractVehicles != null && listContractVehicles.Entities.Count > 0 && !string.IsNullOrEmpty(sin))//
                        {
                            contractIdSet = new string[listContractVehicles.Entities.Count];
                            int contractVehicleCount = 0;
                            foreach (var contractVehicle in listContractVehicles.Entities)
                            {
                                if (contractVehicle.Attributes.Contains("new_tm_toma__sins__c"))
                                {
                                    if (contractVehicle.Attributes["new_tm_toma__sins__c"] != null)
                                    {
                                        if (contractVehicle.Attributes["new_tm_toma__sins__c"].ToString() != "RETURN ALL")
                                        {
                                            if (contractVehicle.Attributes["new_tm_toma__sins__c"].ToString().Contains(sin))
                                            {
                                                string C_Id = contractVehicle.Attributes["new_contractvehiclesid"].ToString();
                                                contractIdSet.SetValue(C_Id, contractVehicleCount);
                                                //contractIdSet.Add(new Guid(contractVehicle.Attributes["new_contractvehiclesid"].ToString()));
                                                //contractIdSet.Add((contractVehicle.Attributes["new_contractvehiclesid"].ToString()));
                                            }
                                            else
                                            {
                                            }
                                        }
                                        else
                                        {
                                            string C_Id = contractVehicle.Attributes["new_contractvehiclesid"].ToString();
                                            contractIdSet.SetValue(C_Id, contractVehicleCount);
                                            //contractIdSet.Add((contractVehicle.Attributes["new_contractvehiclesid"].ToString()));
                                        }
                                        contractVehicleCount = contractVehicleCount + 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                        }
                        #region taskOrdersList

                        //QueryExpression queryTaskOrders = new QueryExpression();
                        //queryTaskOrders.EntityName = "fedcap_taskorder";
                        //queryTaskOrders.ColumnSet = new ColumnSet();
                        //queryTaskOrders.ColumnSet.Columns.Add("fedcap_name");
                        //queryTaskOrders.ColumnSet.Columns.Add("fedcap_contractvehiclename");
                        //queryTaskOrders.ColumnSet.Columns.Add("fedcap_taskordernumber");
                        //queryTaskOrders.Criteria = new FilterExpression();
                        //queryTaskOrders.Criteria.AddCondition("fedcap_taskordernumber", ConditionOperator.Equal, rfqId);
                        //queryTaskOrders.Criteria.AddCondition("fedcap_contractvehiclename", ConditionOperator.In, contractIdSet);
                        //EntityCollection taskOrdersList = organizationService.RetrieveMultiple(queryTaskOrders);
                        #endregion

                        rfqId = Regex.Replace(rfqId, @"\s+", " ", RegexOptions.Multiline);
                        rfqId_Original = rfqId.Trim();
                        rfqId = rfqId.Trim().Split(' ')[0].Trim();
                        rfqId = rfqId.Replace(" ", "");
                        if (contractIdSet != null)
                        {
                            foreach (var contactVehicleId in contractIdSet)
                            {
                                if (contactVehicleId != null)
                                {
                                    QueryExpression queryTaskOrdersCheck = new QueryExpression();
                                    queryTaskOrdersCheck.EntityName = "fedcap_taskorder";
                                    queryTaskOrdersCheck.ColumnSet = new ColumnSet();
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_name");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_contractvehiclename");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskordernumber");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskordertitle");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_rfptoreleasedate");//rFQIssueDt
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_questionsduedate");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_duedate");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_proposalduedate");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_delivery");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_description");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_reference");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_category");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_categorybackend");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_contractingactivity");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_individualreceivingshipment");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_input_data");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_buyer");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_customerorganization");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskorderid");
                                    queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_contractingagencytxt");
                                    
                                    queryTaskOrdersCheck.Criteria = new FilterExpression();
                                    queryTaskOrdersCheck.Criteria.AddCondition("fedcap_taskordernumber", ConditionOperator.Equal, rfqId);
                                    queryTaskOrdersCheck.Criteria.AddCondition("fedcap_contractvehiclename", ConditionOperator.Equal, new Guid(contactVehicleId.ToString()));
                                    EntityCollection oldTaskOrdersList = organizationService.RetrieveMultiple(queryTaskOrdersCheck);
                                    if (rfqTitle != null)
                                    {
                                        rfqTitle = rfqTitle.Trim();
                                    }
                                    if (delivery != null)
                                    {
                                        delivery = delivery.Trim();
                                    }
                                    if (description != null)
                                    {
                                        description = description.Trim();
                                    }
                                    string trimmedDescription = string.Empty;
                                    if (description != null)
                                    {
                                        //description= description.trim();
                                        if (description.Length > 10000)
                                        {
                                            trimmedDescription = description.Substring(0, 10000);
                                        }
                                        else
                                        {
                                            trimmedDescription = description;
                                        }
                                    }
                                    if (reference != null)
                                    {
                                        reference = reference.Trim();
                                    }
                                    else
                                    {
                                        reference = string.Empty;
                                    }
                                    if (category != null)
                                    {
                                        category = category.Trim();
                                        category = checkValidString(category, 254).Trim();
                                    }
                                    if (taskOrderContractingActivity != null)
                                    {
                                        taskOrderContractingActivity = taskOrderContractingActivity.Trim();
                                        taskOrderContractingActivity = checkValidString(category, 255);
                                        //taskOrderContractingActivity = EmailHandlerHelper.checkValidString(taskOrderContractingActivity, 255);
                                    }
                                    if (individualReceivingShipment != null)
                                    {
                                        //individualReceivingShipment = individualReceivingShipment.trim();
                                        individualReceivingShipment = checkValidString(individualReceivingShipment, 255);
                                    }
                                    if (oldTaskOrdersList.Entities.Count > 0)
                                    {
                                        //Update Task Order
                                        foreach (Entity oldTaskOrder in oldTaskOrdersList.Entities)
                                        {
                                            updateFlag = false;
                                            Guid oldTaskOrdersGuid = new Guid(oldTaskOrder.Attributes["fedcap_taskorderid"].ToString());
                                            oldTaskOrdersGuidGbol = new Guid(oldTaskOrder.Attributes["fedcap_taskorderid"].ToString());
                                            ColumnSet attributes = new ColumnSet(new string[] { "fedcap_name", "fedcap_taskorderid",
                                                                                                "fedcap_customerorganization","fedcap_buyer",
                                                                                                "fedcap_taskordertitle","fedcap_rfptoreleasedate",
                                                                                                "fedcap_duedate","fedcap_proposalduedate",
                                                                                                "fedcap_delivery","fedcap_description",
                                                                                                "fedcap_reference","fedcap_category",
                                                                                                "fedcap_categorybackend","fedcap_contractingactivity",
                                                                                                "fedcap_individualreceivingshipment","fedcap_input_data",
                                                                                                "fedcap_contractingagencytxt"
                                                                                                });
                                            //Entity oldTaskOrders = organizationService.Retrieve("fedcap_taskorder", oldTaskOrdersGuid, attributes);

                                            Entity fedcap_taskorder = new Entity("fedcap_taskorder");
                                            fedcap_taskorder.Id = oldTaskOrdersGuid;
                                            Guid fedcap_contractvehiclenumber = new Guid(contactVehicleId.ToString());
                                            fedcap_taskorder["fedcap_taskordernumber"] = rfqId.Trim();
                                            fedcap_taskorder["fedcap_name"] = rfqId_Original.Trim();
                                            fedcap_taskorder["fedcap_contractvehiclename"] = new EntityReference(id: fedcap_contractvehiclenumber, logicalName: "new_contractvehicles");
                                            string rfqTitleOld = string.Empty;
                                            string rfqIdOld =  oldTaskOrder.Attributes["fedcap_taskordernumber"].ToString();
                                            if (oldTaskOrder.Attributes.Contains("fedcap_taskordertitle"))
                                            {
                                                rfqTitleOld = oldTaskOrder.Attributes["fedcap_taskordertitle"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_taskordertitle"].ToString() != rfqTitle) && (oldTaskOrder.Attributes["fedcap_taskordertitle"] != null || rfqTitle != string.Empty) && (rfqTitle != null || oldTaskOrder.Attributes["fedcap_taskordertitle"].ToString() != string.Empty))
                                                {
                                                    updateFlag = true;
                                                    fedcap_taskorder.Attributes["fedcap_taskordertitle"] = rfqTitle;
                                                }
                                            }
                                            else
                                            {
                                                updateFlag = true;
                                                fedcap_taskorder.Attributes["fedcap_taskordertitle"] = rfqTitle;
                                            }
                                            string rFQIssueDateOld = string.Empty;
                                            //*************************fedcap_duedate*******************************************
                                            if (oldTaskOrder.Attributes.Contains("fedcap_duedate"))
                                            {
                                                rFQIssueDateOld = oldTaskOrder.Attributes["fedcap_duedate"].ToString();
                                                
                                                if (!string.IsNullOrEmpty(rFQIssueDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQIssueDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQIssueDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQIssueDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));

                                                            //HttpContext.Current.Response.Write("Converted Existed Date  " + value + " to " + dtrFQIssueDate + ", Kind " + dtrFQIssueDate.Kind);
                                                            fedcap_taskorder.Attributes["fedcap_rfptoreleasedate"] = dt;// (dtrFQIssueDate);
                                                            fedcap_taskorder.Attributes["fedcap_duedate"] = dt;// (dtrFQIssueDate); ;
                                                            //fedcap_taskorder.Attributes["fedcap_proposalduedate"] = dt;// (dtrFQIssueDate);

                                                            if (rFQIssueDateOld.Equals(dt.ToString()))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                updateFlag = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                        //DateTime dtrFQIssueDate = Convert.ToDateTime(rFQIssueDate);
                                                        //fedcap_taskorder.Attributes["fedcap_rfptoreleasedate"] = dtrFQIssueDate;
                                                        //fedcap_taskorder.Attributes["fedcap_duedate"] = dtrFQIssueDate;
                                                        //fedcap_taskorder.Attributes["fedcap_proposalduedate"] = dtrFQIssueDate;
                                                        //updateFlag = true;
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Helper.WriteToLogAndConsole(ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_rfptoreleasedate"] = null;
                                                    fedcap_taskorder.Attributes["fedcap_duedate"] = null;
                                                    //fedcap_taskorder.Attributes["fedcap_proposalduedate"] = null;
                                                    //updateFlag = true;
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(rFQIssueDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQIssueDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQIssueDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQIssueDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));

                                                            fedcap_taskorder.Attributes["fedcap_rfptoreleasedate"] = (dt);
                                                            fedcap_taskorder.Attributes["fedcap_duedate"] = (dt);
                                                            //fedcap_taskorder.Attributes["fedcap_proposalduedate"] = (dt);
                                                            updateFlag = true;
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //HttpContext.Current.Response.Write(ex.Message.ToString());
                                                        Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder - rFQIssueDate:- " + ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_rfptoreleasedate"] = null;
                                                    fedcap_taskorder.Attributes["fedcap_duedate"] = null;
                                                    //fedcap_taskorder.Attributes["fedcap_proposalduedate"] = null;
                                                }
                                            }
                                            //**************************end fedcap_duedate****************************************
                                            //*************************start fedcap_proposalduedate*******************************************
                                            string rFQCloseDateOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_proposalduedate"))
                                            {
                                                rFQCloseDateOld = oldTaskOrder.Attributes["fedcap_proposalduedate"].ToString();

                                                if (!string.IsNullOrEmpty(rFQCloseDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQCloseDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQCloseDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQCloseDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));
                                                            fedcap_taskorder.Attributes["fedcap_proposalduedate"] = dt;// (dtrFQIssueDate);
                                                            if (rFQCloseDateOld.Equals(dt.ToString()))
                                                            {
                                                            }
                                                            else
                                                            {
                                                                updateFlag = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Helper.WriteToLogAndConsole(ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_proposalduedate"] = null;
                                                    //updateFlag = true;
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(rFQCloseDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQCloseDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQCloseDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQCloseDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));

                                                            fedcap_taskorder.Attributes["fedcap_proposalduedate"] = (dt);
                                                            updateFlag = true;
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //HttpContext.Current.Response.Write(ex.Message.ToString());
                                                        Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder - rFQCloseDate:- " + ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_proposalduedate"] = null;
                                                }
                                            }
                                            //**************************end fedcap_proposalduedate****************************************
                                            string deliveryOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_delivery"))
                                            {
                                                deliveryOld = oldTaskOrder.Attributes["fedcap_delivery"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_delivery"].ToString() != delivery) && (oldTaskOrder.Attributes["fedcap_delivery"] != null || delivery != string.Empty) && (delivery != null || oldTaskOrder.Attributes["fedcap_delivery"].ToString() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_delivery"] = delivery;
                                                    updateFlag = true;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_delivery"] = delivery;
                                                updateFlag = true;
                                            }
                                            string trimmedDescriptionOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_description"))
                                            {
                                                trimmedDescriptionOld = oldTaskOrder.Attributes["fedcap_description"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_description"].ToString() != trimmedDescription) && (oldTaskOrder.Attributes["fedcap_description"] != null || trimmedDescription != string.Empty) && (trimmedDescription != null || oldTaskOrder.Attributes["fedcap_description"].ToString() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_description"] = trimmedDescription;
                                                    updateFlag = true;
                                                    

                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_description"] = trimmedDescription;
                                                if (!string.IsNullOrEmpty(trimmedDescription))
                                                {
                                                    updateFlag = true;
                                                }
                                            }
                                            string referenceOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_reference"))
                                            {
                                                referenceOld = oldTaskOrder.Attributes["fedcap_reference"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_reference"].ToString() != reference) && (oldTaskOrder.Attributes["fedcap_reference"] != null || reference != string.Empty) && (reference != null || oldTaskOrder.Attributes["fedcap_reference"].ToString() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_reference"] = reference;
                                                    updateFlag = true;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_reference"] = reference;
                                                if (!string.IsNullOrEmpty(reference))
                                                {
                                                    updateFlag = true;
                                                }
                                            }

                                            string categoryOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_category"))
                                            {
                                                categoryOld = oldTaskOrder.Attributes["fedcap_category"].ToString().Trim();
                                                if ((oldTaskOrder.Attributes["fedcap_category"].ToString().Trim() != category.Trim()) && (oldTaskOrder.Attributes["fedcap_category"] != null || category != string.Empty) && (category != null || oldTaskOrder.Attributes["fedcap_category"].ToString().Trim() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_category"] = category.Trim();// checkValidString(category, 254).Trim();
                                                    updateFlag = true;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_category"] = category.Trim(); //checkValidString(category, 254).Trim();
                                                updateFlag = true;
                                            }
                                            string categorybackendOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_categorybackend"))
                                            {
                                                categorybackendOld = oldTaskOrder.Attributes["fedcap_categorybackend"].ToString().Trim();
                                                if ((oldTaskOrder.Attributes["fedcap_categorybackend"].ToString().Trim() != category.Trim()) && (oldTaskOrder.Attributes["fedcap_categorybackend"] != null || category != string.Empty) && (category != null || oldTaskOrder.Attributes["fedcap_categorybackend"].ToString().Trim() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_categorybackend"] = category.Trim(); //checkValidString(category, 254).Trim();
                                                    updateFlag = true;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_categorybackend"] = category.Trim(); //checkValidString(category, 254).Trim();
                                                updateFlag = true;
                                            }
                                            //Guid buyerId = getContact();
                                            //if ((taskOrder.Buyer__c != buyerId))
                                            //{
                                            //    taskOrder.Buyer__c = buyerId;
                                            //}
                                            //if ((acc != null && taskOrder.Buyer__c != acc.id))
                                            //{
                                            //    taskOrder.Customer_Agency__c = acc.id;
                                            //}
                                            string buyerIdOld = string.Empty;
                                            string buyerId = GetContact(organizationService);
                                            string accountIdOld = string.Empty;
                                            string accountId = GetAccount(organizationService);
                                            string contractingAgency = GetContractingAgency(organizationService); ;
                                            string contractingAgencyOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_contractingagencytxt"))
                                            {
                                                contractingAgencyOld = oldTaskOrder.Attributes["fedcap_contractingagencytxt"].ToString().Trim();
                                                if ((oldTaskOrder.Attributes["fedcap_contractingagencytxt"].ToString().Trim() != contractingAgency.Trim()) && (oldTaskOrder.Attributes["fedcap_contractingagencytxt"] != null || contractingAgency != string.Empty) && (contractingAgency != null || oldTaskOrder.Attributes["fedcap_contractingagencytxt"].ToString().Trim() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_contractingagencytxt"] = contractingAgency.Trim();
                                                    updateFlag = true;

                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_contractingagencytxt"] = contractingAgency.Trim();
                                                updateFlag = true;
                                            }
                                            try
                                            {
                                                if (oldTaskOrder.Attributes.Contains("fedcap_buyer"))
                                                {
                                                    //buyerIdOld = oldTaskOrder.Attributes["fedcap_buyer"].ToString();
                                                    EntityReference entityRef = (EntityReference)oldTaskOrder.Attributes["fedcap_buyer"];
                                                    buyerIdOld = entityRef.Id.ToString();
                                                    if (!buyerId.Equals(buyerIdOld))
                                                    {
                                                        if (!string.IsNullOrEmpty(buyerId))
                                                        {
                                                            fedcap_taskorder.Attributes["fedcap_buyer"] = new EntityReference(id: new Guid(buyerId), logicalName: "contact");
                                                        }
                                                        else
                                                        {
                                                            fedcap_taskorder.Attributes["fedcap_buyer"] = null;
                                                        }
                                                        updateFlag = true;
                                                    }
                                                }
                                                else
                                                {
                                                    //EntityReference entityRef = (EntityReference)oldTaskOrder.Attributes["fedcap_buyer"];
                                                    if (!string.IsNullOrEmpty(buyerId))
                                                    {
                                                        fedcap_taskorder.Attributes["fedcap_buyer"] = new EntityReference(id: new Guid(buyerId), logicalName: "contact");
                                                    }
                                                    else
                                                    {
                                                        fedcap_taskorder.Attributes["fedcap_buyer"] = null;
                                                    }
                                                    updateFlag = true;
                                                }

                                                if (oldTaskOrder.Attributes.Contains("fedcap_customerorganization"))
                                                {
                                                    //accountIdOld = oldTaskOrder.Attributes["fedcap_customerorganization"].ToString();
                                                    EntityReference entityRef = (EntityReference)oldTaskOrder.Attributes["fedcap_customerorganization"];
                                                    accountIdOld = entityRef.Id.ToString();
                                                    if (!accountId.Equals(accountIdOld))
                                                    {
                                                        if (!string.IsNullOrEmpty(accountId))
                                                        {
                                                            fedcap_taskorder.Attributes["fedcap_customerorganization"] = new EntityReference(id: new Guid(accountId), logicalName: "account");
                                                        }
                                                        else
                                                        {
                                                            fedcap_taskorder.Attributes["fedcap_customerorganization"] = null;
                                                        }
                                                        //updateFlag = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(accountId))
                                                    {
                                                        fedcap_taskorder.Attributes["fedcap_customerorganization"] = new EntityReference(id: new Guid(accountId), logicalName: "account");
                                                    }
                                                    else
                                                    {
                                                        fedcap_taskorder.Attributes["fedcap_customerorganization"] = null;
                                                    }
                                                    //updateFlag = true;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder TaskOrderId- " + rfqId +" - fedcap_buyer - " + buyerId + " - fedcap_customerorganization - " + accountId + "" + ex.Message.ToString());
                                            }

                                            string taskOrderContractingActivityOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_contractingactivity"))
                                            {
                                                taskOrderContractingActivityOld = oldTaskOrder.Attributes["fedcap_contractingactivity"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_contractingactivity"].ToString() != taskOrderContractingActivity) && (oldTaskOrder.Attributes["fedcap_contractingactivity"] != null || taskOrderContractingActivity != string.Empty) && (taskOrderContractingActivity != null || oldTaskOrder.Attributes["fedcap_contractingactivity"].ToString() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_contractingactivity"] = taskOrderContractingActivity;
                                                    updateFlag = true;
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_contractingactivity"] = taskOrderContractingActivity;
                                                updateFlag = true;
                                            }

                                            string individualReceivingShipmentOld = string.Empty;
                                            if (oldTaskOrder.Attributes.Contains("fedcap_individualreceivingshipment"))
                                            {
                                                individualReceivingShipmentOld = oldTaskOrder.Attributes["fedcap_individualreceivingshipment"].ToString();
                                                if ((oldTaskOrder.Attributes["fedcap_individualreceivingshipment"].ToString() != individualReceivingShipment) && (oldTaskOrder.Attributes["fedcap_individualreceivingshipment"] != null || individualReceivingShipment != string.Empty) && (individualReceivingShipment != null || oldTaskOrder.Attributes["fedcap_individualreceivingshipment"].ToString() != string.Empty))
                                                {
                                                    fedcap_taskorder.Attributes["fedcap_individualreceivingshipment"] = individualReceivingShipment;
                                                    updateFlag = true;
                                                    
                                                }
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_individualreceivingshipment"] = individualReceivingShipment;
                                                updateFlag = true;
                                            }

                                            if (htmlBody.Length < 1048576)
                                            {//1,048,576 CRM
                                                fedcap_taskorder.Attributes["fedcap_input_data"] = htmlBody;
                                            }
                                            else
                                            {
                                                fedcap_taskorder.Attributes["fedcap_input_data"] = htmlBody.Substring(0, 1048576);
                                            }
                                            organizationService.Update(fedcap_taskorder);


                                            if (updateFlag)
                                            {
                                                //GSAParseHelper.InsertContractMods(oldTaskOrdersGuid.ToString(), rfqId, rfqTitle, rFQIssueDate, delivery,
                                                //                                   trimmedDescription, reference, checkValidString(category, 254),
                                                //                                   checkValidString(category, 254), taskOrderContractingActivity, individualReceivingShipment
                                                //                                   , rfqIdOld, rfqTitleOld, rFQIssueDateOld, deliveryOld, trimmedDescriptionOld, referenceOld,
                                                //                                   categoryOld, categorybackendOld, taskOrderContractingActivityOld, individualReceivingShipmentOld
                                                //                                   ,buyerId,buyerIdOld,accountId,accountIdOld);

                                                GSAParseHelper.InsertContractMods(oldTaskOrdersGuid.ToString(), rfqId, rfqTitle, rFQIssueDate, delivery,
                                                                                       trimmedDescription, reference, category.Trim(),
                                                                                       category.Trim(), taskOrderContractingActivity, individualReceivingShipment
                                                                                       , rfqIdOld, rfqTitleOld, rFQIssueDateOld, deliveryOld, trimmedDescriptionOld, referenceOld,
                                                                                       categoryOld, categorybackendOld, taskOrderContractingActivityOld, individualReceivingShipmentOld
                                                                                       , buyerId, buyerIdOld, accountId, accountIdOld,contractingAgency,contractingAgencyOld,rFQCloseDate,rFQCloseDateOld);
                                            }
                                            //returnURL = "Contract_Number :-->" + contractNumber + " RFQ_ID :-->" + rfqId + " updated.";
                                        }
                                    }
                                    else
                                    {
                                        // Insert Task Order
                                        Guid fedcap_contractvehiclenumber = new Guid(contactVehicleId.ToString());
                                        Entity newTaskOrderInsert = new Entity("fedcap_taskorder");
                                        newTaskOrderInsert["fedcap_taskordernumber"] = rfqId.Trim();
                                        newTaskOrderInsert["fedcap_name"] = rfqId_Original.Trim();
                                        newTaskOrderInsert["fedcap_contractvehiclename"] = new EntityReference(id: fedcap_contractvehiclenumber, logicalName: "new_contractvehicles");
                                        newTaskOrderInsert.Attributes["fedcap_taskordertitle"] = rfqTitle;
                                        //*****************************Start rFQIssueDate*****************************************
                                        if (!string.IsNullOrEmpty(rFQIssueDate.ToString()))
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(rFQIssueDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQIssueDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQIssueDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQIssueDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));

                                                            newTaskOrderInsert.Attributes["fedcap_rfptoreleasedate"] = (dt);
                                                            newTaskOrderInsert.Attributes["fedcap_duedate"] = (dt); ;
                                                            //newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = (dt);
                                                            updateFlag = true;
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        HttpContext.Current.Response.Write(ex.Message.ToString());
                                                        Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder:- rFQIssueDate(Insert) - " + ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    newTaskOrderInsert.Attributes["fedcap_rfptoreleasedate"] = null;
                                                    newTaskOrderInsert.Attributes["fedcap_duedate"] = null;
                                                    //newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = null;
                                                }
                                                //DateTime dtrFQIssueDate = Convert.ToDateTime(rFQIssueDate);
                                                //newTaskOrderInsert.Attributes["fedcap_rfptoreleasedate"] = dtrFQIssueDate;
                                                //newTaskOrderInsert.Attributes["fedcap_duedate"] = dtrFQIssueDate;
                                                //newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = dtrFQIssueDate;
                                            }
                                            catch (Exception ex)
                                            {
                                                Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder:- rFQIssueDate(Insert2) - " + ex.Message.ToString());
                                            }
                                        }
                                        else
                                        {
                                            newTaskOrderInsert.Attributes["fedcap_rfptoreleasedate"] = null;
                                            newTaskOrderInsert.Attributes["fedcap_duedate"] = null;
                                            //newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = null;
                                        }
                                        //*****************************End rFQIssueDate*****************************************

                                        //*****************************Start rFQCloseDate*****************************************
                                        if (!string.IsNullOrEmpty(rFQCloseDate.ToString()))
                                        {
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(rFQCloseDate.ToString()))
                                                {
                                                    try
                                                    {
                                                        string dateValue = rFQCloseDate;
                                                        string[] split = dateValue.Split(' ');
                                                        string strShortTime = (split[split.Length - 1]);
                                                        string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                                        TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string strTime = tzi.BaseUtcOffset.ToString();
                                                        dateValue = dateValue + strTime;
                                                        dateValue = dateValue.Remove(dateValue.Length - 3);
                                                        string value = dateValue;
                                                        string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                                        DateTime dtrFQCloseDate;
                                                        if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQCloseDate))
                                                        {
                                                            string[] splitInt = split[0].Split('/');
                                                            DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));

                                                            newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = (dt);
                                                            updateFlag = true;
                                                        }
                                                        else
                                                        {
                                                            Helper.WriteToLogAndConsole("Unable to convert date: - " + value);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        HttpContext.Current.Response.Write(ex.Message.ToString());
                                                        Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder:- rFQCloseDate(Insert) - " + ex.Message.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = null;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder:- rFQCloseDate(Insert2) - " + ex.Message.ToString());
                                            }
                                        }
                                        else
                                        {
                                            newTaskOrderInsert.Attributes["fedcap_proposalduedate"] = null;
                                        }
                                        //*****************************End rFQCloseDate*****************************************

                                        newTaskOrderInsert.Attributes["fedcap_delivery"] = delivery;
                                        newTaskOrderInsert.Attributes["fedcap_description"] = trimmedDescription;
                                        newTaskOrderInsert.Attributes["fedcap_reference"] = reference;
                                        newTaskOrderInsert.Attributes["fedcap_category"] = category.Trim();//checkValidString(category, 254);
                                        newTaskOrderInsert.Attributes["fedcap_categorybackend"] = category.Trim(); //checkValidString(category, 254);
                                        //Guid buyerId = getContact();
                                        //if ((taskOrder.Buyer__c != buyerId))
                                        //{
                                        //    taskOrder.Buyer__c = buyerId;
                                        //}
                                        //if ((acc != null && taskOrder.Buyer__c != acc.id))
                                        //{
                                        //    taskOrder.Customer_Agency__c = acc.id;
                                        //}

                                        string buyerIdOld = string.Empty;
                                        string buyerId = GetContact(organizationService);
                                        string accountIdOld = string.Empty;
                                        string accountId = GetAccount(organizationService);
                                        string contractingAgency = GetContractingAgency(organizationService);
                                        newTaskOrderInsert.Attributes["fedcap_contractingagencytxt"] = contractingAgency.Trim();
                                        
                                        try
                                        {
                                            if (!string.IsNullOrEmpty(buyerId))
                                            {
                                                newTaskOrderInsert.Attributes["fedcap_buyer"] = new EntityReference(id: new Guid(buyerId), logicalName: "contact");
                                            }
                                            else
                                            {
                                                newTaskOrderInsert.Attributes["fedcap_buyer"] = null;
                                            }
                                            if (!string.IsNullOrEmpty(accountId))
                                            {
                                                newTaskOrderInsert.Attributes["fedcap_customerorganization"] = new EntityReference(id: new Guid(accountId), logicalName: "account");
                                            }
                                            else
                                            {
                                                newTaskOrderInsert.Attributes["fedcap_customerorganization"] = null;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder - TaskOrderId- " + rfqId + " fedcap_customerorganization " + accountId + " - fedcap_buyer " + buyerId+ " (Insert) - " + ex.Message.ToString());
                                        }

                                        newTaskOrderInsert.Attributes["fedcap_contractingactivity"] = taskOrderContractingActivity;
                                        newTaskOrderInsert.Attributes["fedcap_individualreceivingshipment"] = individualReceivingShipment;
                                        if (htmlBody.Length < 1048576)
                                        {//1,048,576 CRM
                                            newTaskOrderInsert.Attributes["fedcap_input_data"] = htmlBody;
                                        }
                                        else
                                        {
                                            newTaskOrderInsert.Attributes["fedcap_input_data"] = htmlBody.Substring(0, 1048576);
                                        }
                                       newTaskOrder =  organizationService.Create(newTaskOrderInsert);
                                        //GSAParseHelper.InsertContractMods(newTaskOrder.ToString(), rfqId.Trim(), rfqTitle.Trim(), rFQIssueDate, delivery.Trim()
                                        //                                   , trimmedDescription.Trim(), reference.Trim(), checkValidString(category, 254), checkValidString(category, 254)
                                        //                                   , taskOrderContractingActivity, individualReceivingShipment);
                                        //returnURL = "Contract_Number :-->" + contractNumber + " RFQ_ID :-->" + rfqId + "rFQIssueDate:--> " + rFQIssueDate + " inserted.";
                                    }
                                    if (oldTaskOrdersList.Entities.Count > 0)
                                    {
                                        foreach (Entity oldTaskOrder in oldTaskOrdersList.Entities)
                                        {
                                            if (oldTaskOrder.Id != null)
                                            {
                                                buyerDocumentsUrlsComplete += parseAttachmentURL(htmlBody, oldTaskOrder.Id.ToString());
                                                modificationURLComplete += parseModificationURL(htmlBody, oldTaskOrder.Id.ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (newTaskOrder != null)
                                        {
                                            buyerDocumentsUrlsComplete += parseAttachmentURL(htmlBody, newTaskOrder.ToString());
                                            modificationURLComplete += parseModificationURL(htmlBody, newTaskOrder.ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    //returnURL = "Contract_Number :-->" + contractNumber + " RFQ_ID :-->" + rfqId + " not exist";
                                }
                            }
                        }
                        else
                        {
                            //returnURL = "Contract_Number :-->" + contractNumber + " not exist";
                        }
                    }
                }
                else
                {
                    //returnURL += "usernot login";
                }
                //HttpContext.Current.Response.Write("Contract_Number :-->" + contractNumber + " RFQ_ID :-->" + rfqId);
                //returnURL = "MODIFICATION-" + rfqId + "rFQIssueDate:- " + rFQIssueDate;
            }
            catch (Exception ex)
            {
                //HttpContext.Current.Response.Write(e.Message.ToString());
                returnURL = ex.Message.ToString() + " RFQ_ID :-->" + rfqId + "Guid:-->" + oldTaskOrdersGuidGbol.ToString();
                Helper.WriteToLogAndConsole("UpsertGSADataInTaskOrder:- " + returnURL);
            }

            return buyerDocumentsUrlsComplete + "MODIFICATION" + modificationURLComplete;
        }
        private string parseAttachmentURL(string htmlBody, string taskOrderId)
        {
            try
            {
                string newAttachmentHtml = htmlBody.SubStringBetween("Buyer Documents:", "Quote ID");
                if (string.IsNullOrEmpty(newAttachmentHtml))
                {
                    newAttachmentHtml = htmlBody.SubStringBetween("Buyer Documents:", "Mfr. Part/Item #");
                }
                if (!string.IsNullOrEmpty(newAttachmentHtml))
                {
                    string[] rows = newAttachmentHtml.Split('\n');
                    foreach (var row in rows)
                    {
                        bool isContains = row.Contains("<a href=");
                        bool flag = true;
                        if (isContains)
                        {
                            bool isContainshref = row.Contains("href=");
                            if (isContainshref)
                            {
                                string documentName = row.StripHtmlTags();
                                string tempUrl = row.SubStringBetween("href=", ">").Trim();
                                tempUrl = tempUrl.Replace("\'", string.Empty);
                                tempUrl = tempUrl.Replace("\"", string.Empty);
                                tempUrl = tempUrl.Replace("target=showDoc", string.Empty);
                                int index1 = tempUrl.IndexOf("filename=");
                                int index2 = tempUrl.IndexOf("&path");
                                string fileName;
                                if (index2 == -1)
                                {
                                    index2 = tempUrl.IndexOf("target=");
                                    fileName = tempUrl.SubStringBetween("filename=", "target=").Trim();
                                }
                                else
                                {
                                    fileName = tempUrl.SubStringBetween("filename=", "&path").Trim();
                                }
                                
                                if (fileName != null && fileName.Trim().Length > 0)
                                {
                                    // String fileName = tempUrl.subString(index1,index2);
                                    int index3 = fileName.LastIndexOf('.');
                                    if (index3 != -1)
                                    {
                                        string fileExtension = fileName.Substring(index3);
                                        int extIndex = documentName.LastIndexOf('.');
                                        if (extIndex != -1 && documentName.Substring(extIndex).Trim() == fileExtension.Trim())
                                        {
                                            fileExtension = string.Empty;
                                        }
                                        if (tempUrl.Contains("/advantage/ebuy/view_doc.do"))
                                        {
                                            tempUrl = "https://www.ebuy.gsa.gov" + tempUrl;
                                        }

                                        buyerDocumentsUrls += tempUrl + "SPLITDATA" + documentName.Trim() + fileExtension + "SPLITDATA" + taskOrderId + "SPLITURL";
                                        flag = false;
                                    }
                                }
                                if (tempUrl.Contains("Files/") && flag)
                                {
                                    tempUrl = tempUrl.Replace("target=showdoc", string.Empty).Trim();
                                    int lastIndex = tempUrl.LastIndexOf('/');
                                    //Integer lastIndex2 = tempUrl.lastIndexOf('target=');
                                    if (lastIndex != -1)
                                    {
                                        flag = false;
                                        documentName = tempUrl.Substring(lastIndex + 1);
                                        buyerDocumentsUrls += tempUrl + "SPLITDATA" + documentName + "SPLITDATA" + taskOrderId + "SPLITURL";
                                    }
                                }
                                if (flag)
                                {
                                    buyerDocumentsUrls += tempUrl + "SPLITDATA" + documentName + "SPLITDATA" + taskOrderId + "SPLITURL";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("parseAttachmentURL() - TaskOrderId:- " + taskOrderId + " - " + ex.Message.ToString());
            }
            return buyerDocumentsUrls;
        }
        private string parseModificationURL(string htmlBody, string taskOrderId)
        {
            string modificationData = string.Empty;
            try
            {

                string tempHtml = htmlBody.SubStringBetween("RFQ ID:", "Reference #:").Trim();
                string[] stringSeparators = new string[] { "</a>" };
                string[] rows = tempHtml.Split(stringSeparators, StringSplitOptions.None);//</a>

                foreach (var row in rows)
                {
                    try
                    {
                        bool isContains2 = row.Contains("/advantage/ebuy/seller/RFQ_modification_description.do");
                        bool isContains = row.Contains("/advantage/seller/RFQ_modification_description.do");
                        bool isContains1 = row.Contains("/advantage/seller/view_qandas.do");
                        if (isContains || isContains1 || isContains2)
                        {
                            bool isContainshref = row.Contains("href=");
                            if (isContainshref)
                            {
                                string modificationName = row.StripHtmlTags().Replace("\t", string.Empty).Trim();
                                int mdIndex = modificationName.IndexOf("Modification");
                                if (mdIndex == -1)
                                {
                                    mdIndex = modificationName.IndexOf("Q&A");
                                }
                                if (mdIndex != -1)
                                {
                                    modificationName = modificationName.Substring(mdIndex);
                                }
                                string[] stringSeparatorsMod = new string[] { "Modification " };

                                string[] modificationSplit = null;
                                modificationSplit = modificationName.Split(stringSeparatorsMod, StringSplitOptions.None);
                                string url = string.Empty;
                                if (modificationSplit.Length > 0)
                                {
                                    if (isContains)
                                    {
                                        url = "/advantage/seller/RFQ_modification_description.do?&rfqId=" + rfqId.Trim() + "&versionNumber=" + modificationSplit[1].Trim() + "&fromPage=/seller/prepareQuote.do";
                                    }
                                    else if (isContains1)
                                    {
                                        url = "/advantage/seller/view_qandas.do?&rfqId=" + rfqId.Trim() + "&versionNumber=" + modificationSplit[1].Trim() + "&fromPage=/seller/prepareQuote.do";
                                    }
                                    else if (isContains2)
                                    {
                                        url = "/advantage/ebuy/seller/RFQ_modification_description.do?&rfqId=" + rfqId.Trim() + "&versionNumber=" + modificationSplit[1].Trim() + "&fromPage=/seller/prepareQuote.do";
                                    }
                                    //string tempUrl = row.SubStringBetween("href=", ">").Replace("\t", string.Empty).Trim();
                                    //tempUrl = tempUrl.Replace("\'", string.Empty);
                                    //tempUrl = tempUrl.Replace("\"", string.Empty);
                                    ////tempUrl = tempUrl.Replace('"', "");
                                    modificationData += url + "SPLITDATA" + modificationName + "SPLITDATA" + taskOrderId + "SPLITURL";
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.WriteToLogAndConsole("parseModificationURL() In Foreach Loop - rfqId:- " + rfqId + " - " + ex.Message.ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                Helper.WriteToLogAndConsole("parseModificationURL() - taskOrderId:- " + taskOrderId + " - " + ex.Message.ToString());
            }
            return modificationData;
        }
        private string checkValidString(string str, int endLimit)
        {
            try
            {
                if (str.Length > endLimit)
                {
                    String validString = str.Substring(0, endLimit);
                    return validString.Trim().Replace("\t", "").Trim();
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("checkValidString " + ex.Message.ToString());
            }
            return str.Trim().Replace("\t", "").Trim();
        }
        //private Nullable<DateTime> getFormattedDateTime(string rawDate)
        //{
        //    if (rawDate != null)
        //    {
        //        string[] splitDate = rawDate.Split(' ');
        //        string d = splitDate[0];
        //        string t = splitDate[1];
        //        t = t.Substring(0, 5);
        //        string ampm = splitDate[2];
        //        string timeFormat = splitDate[3];
        //        int millisecondsDiff = DateTimeHelper.getTimeOffset(timeFormat);

        //        int second = millisecondsDiff / 1000;
        //        int minute = millisecondsDiff / (1000 * 60);
        //        int hours = millisecondsDiff / (1000 * 60 * 60);
        //        int remainingMinute = 0;
        //        if (minute != null && hours != null && minute != 0 && hours != 0)
        //        {
        //            remainingMinute = Math.mod(minute, hours);
        //        }

        //        DateTime dt = DateTime.Parse(d + ' ' + t + ' ' + ampm);
        //        dt = dt.AddHours(hours * -1);
        //        if (remainingMinute != null && remainingMinute != 0)
        //        {
        //            dt = dt.AddMinutes(remainingMinute * -1);
        //        }
        //        return dt;
        //    }
        //    return null;
        //}
        private string GetContractingAgency(IOrganizationService organizationService)
        {
            string name = string.Empty;
            try
            {
                string[] contactDetails = contact.Split('\n');
                int size = contactDetails.Length;
                name = contactDetails[size - 3];
                name = name.Trim();
                name = Regex.Replace(name, @"\t|\n|\r", "");
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GetContractingAgency() :  " + ex.Message.ToString());
            }
            return name;
        }
        private string GetAccount(IOrganizationService organizationService)
        {
            string Id = string.Empty;
            try
            {
                string[] contactDetails = contact.Split('\n');
                int size = contactDetails.Length;
                string emailId = contactDetails[size - 1];
                string[] emailSplit = emailId.Split('@');
                if (emailSplit.Length > 1)
                {
                    string website = emailSplit[1];
                    string website1 = "http://" + website;
                    string website2 = "https://" + website;
                    string website3 = "www." + website;
                    string website4 = "http://www." + website;
                    string website5 = "https://www." + website;

                    QueryExpression accounts = new QueryExpression("account");

                    accounts.ColumnSet = new ColumnSet("accountid");

                    FilterExpression Filter1 = new FilterExpression(LogicalOperator.Or);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website1);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website2);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website3);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website4);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website5);

                    FilterExpression mainFilter = new FilterExpression(LogicalOperator.Or);
                    mainFilter.AddFilter(Filter1);
                    accounts.Criteria = mainFilter;
                    EntityCollection queryEntityRecords = organizationService.RetrieveMultiple(accounts);

                    //If notes records found
                    if (queryEntityRecords.Entities.Count > 0)
                    {
                        var account = queryEntityRecords.Entities[0];
                        Id = account.Id.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GetAccount() : ID - " + Id + " " + ex.Message.ToString());
            }
            return Id;
        }
        private string GetContact(IOrganizationService organizationService)
        {
            string accountId = string.Empty;
            string contactId = string.Empty;
            try
            {
                string[] contactDetails = contact.Split('\n');
                int size = contactDetails.Length;
                string emailId = contactDetails[size - 1];
                string[] emailSplit = emailId.Split('@');
                if (emailSplit.Length > 1)
                {
                    string website = emailSplit[1];
                    string website1 = "http://" + website;
                    string website2 = "https://" + website;
                    string website3 = "www." + website;
                    string website4 = "http://www." + website;
                    string website5 = "https://www." + website;

                    QueryExpression accounts = new QueryExpression("account");

                    accounts.ColumnSet = new ColumnSet("accountid");

                    FilterExpression Filter1 = new FilterExpression(LogicalOperator.Or);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website1);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website2);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website3);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website4);
                    Filter1.AddCondition("websiteurl", ConditionOperator.Equal, website5);

                    FilterExpression mainFilter = new FilterExpression(LogicalOperator.Or);
                    mainFilter.AddFilter(Filter1);
                    accounts.Criteria = mainFilter;
                    EntityCollection queryEntityRecords = organizationService.RetrieveMultiple(accounts);

                    if (queryEntityRecords.Entities.Count > 0)
                    {
                        var account = queryEntityRecords.Entities[0];
                        accountId = account.Id.ToString();
                    }
                    QueryExpression queryContacts = new QueryExpression();
                    queryContacts.EntityName = "contact";
                    queryContacts.ColumnSet = new ColumnSet();
                    queryContacts.ColumnSet.Columns.Add("contactid");
                    queryContacts.ColumnSet.Columns.Add("emailaddress1");
                    queryContacts.Criteria = new FilterExpression();
                    queryContacts.Criteria.AddCondition("emailaddress1", ConditionOperator.Equal, emailId.Trim());
                    EntityCollection ContactsList = organizationService.RetrieveMultiple(queryContacts);
                    if (ContactsList.Entities.Count.Equals(0))
                    {
                        Entity contact = new Entity("contact");
                        if (!string.IsNullOrEmpty(accountId))
                        {
                            contact.Attributes["parentcustomerid"] = new EntityReference(id: new Guid(accountId), logicalName: "account");
                        }
                        string[] name = contactDetails[0].Split(' ');
                        contact.Attributes["firstname"] = name[0].Trim();
                        contact.Attributes["lastname"] = name[1].Trim();
                        contact.Attributes["jobtitle"] = contactDetails[1].Trim();
                        contact.Attributes["emailaddress1"] = contactDetails[3].Trim();//Email is presrnt on 4th location of array 3rd loction is empty
                        contact.Attributes["fedcap_fedtomcontact"] = true;
                        
                        Guid id = organizationService.Create(contact);
                        contactId = id.ToString();
                    }
                    else
                    {
                        contactId = ContactsList.Entities[0].Id.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("GetContact - ID: " + contactId + " " + ex.Message.ToString());
            }
            return contactId;
        }
        private void createContractMods(string rfqId, string contractNumber, string newParserStringSplit, string sins)
        {
            try
            {
                //rfqId = Regex.Replace(rfqId, @"\s+", " ", RegexOptions.Multiline);
                //rfqId = rfqId.Trim().Split(' ')[0].Trim();
                //rfqId = rfqId.Replace(" ", "");

                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();

                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);

                if (organizationService == null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(rfqId) && !string.IsNullOrEmpty(contractNumber) && !string.IsNullOrEmpty(newParserStringSplit))
                {

                    rfqId = rfqId.Replace(" ", string.Empty).Trim();
                    contractNumber = contractNumber.Replace(" ", string.Empty).Trim();
                    newParserStringSplit = newParserStringSplit.Replace(" ", string.Empty).Trim();
                    newParserStringSplit = newParserStringSplit.SubStringBetween("\n", "RESPONSEURL=");
                    QueryExpression queryContractVehicles = new QueryExpression();
                    queryContractVehicles.EntityName = "new_contractvehicles";
                    queryContractVehicles.ColumnSet = new ColumnSet();
                    queryContractVehicles.ColumnSet.Columns.Add("new_name");
                    queryContractVehicles.ColumnSet.Columns.Add("new_tm_toma__contract_number__c");
                    queryContractVehicles.ColumnSet.Columns.Add("new_tm_toma__sins__c");
                    queryContractVehicles.Criteria = new FilterExpression();
                    queryContractVehicles.Criteria.AddCondition("new_tm_toma__contract_number__c", ConditionOperator.Equal, contractNumber);
                    //queryContractVehicles.Criteria.AddCondition("new_tm_toma__sins__c", ConditionOperator.NotEqual, null);
                    EntityCollection listContractVehicles = organizationService.RetrieveMultiple(queryContractVehicles);
                    string[] contractIdSet = null;
                    if (listContractVehicles != null && listContractVehicles.Entities.Count > 0 && !string.IsNullOrEmpty(sins))//
                    {
                        int contractVehicleCount = 0;
                        foreach (var contractVehicle in listContractVehicles.Entities)
                        {
                            if (contractVehicle.Attributes.Contains("new_tm_toma__sins__c"))
                            {
                                if (contractVehicle.Attributes["new_tm_toma__sins__c"] != null)
                                {
                                    if (contractVehicle.Attributes["new_tm_toma__sins__c"].ToString() != "RETURN ALL")
                                    {
                                        if (contractVehicle.Attributes["new_tm_toma__sins__c"].ToString().Contains(sins))
                                        {
                                            string C_Id = contractVehicle.Attributes["new_contractvehiclesid"].ToString();
                                            contractIdSet.SetValue(C_Id, contractVehicleCount);
                                            break;
                                        }
                                    }
                                    contractVehicleCount = contractVehicleCount + 1;
                                }
                            }
                        }
                    }

                    if (contractIdSet != null)
                    {
                        foreach (var contactVehicleId in contractIdSet)
                        {
                            if (contactVehicleId != null)
                            {
                                QueryExpression queryTaskOrdersCheck = new QueryExpression();
                                queryTaskOrdersCheck.EntityName = "fedcap_taskorder";
                                queryTaskOrdersCheck.ColumnSet = new ColumnSet();
                                queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_name");
                                queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_contractvehiclename");
                                queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_taskordernumber");
                                queryTaskOrdersCheck.ColumnSet.Columns.Add("fedcap_is_cancelled");
                                queryTaskOrdersCheck.TopCount = 1;
                                queryTaskOrdersCheck.Criteria = new FilterExpression();
                                queryTaskOrdersCheck.Criteria.AddCondition("fedcap_taskordernumber", ConditionOperator.Equal, rfqId);
                                queryTaskOrdersCheck.Criteria.AddCondition("fedcap_contractvehiclename", ConditionOperator.Equal, new Guid(contactVehicleId.ToString()));
                                EntityCollection oldTaskOrdersList = organizationService.RetrieveMultiple(queryTaskOrdersCheck);
                                if (oldTaskOrdersList.Entities.Count > 0)
                                {
                                    var oldTaskOrder = oldTaskOrdersList.Entities[0];
                                    Entity contractMod = new Entity("fedcap_contractmods");
                                    string modificationName = "Cancelled";
                                    QueryExpression queryContractMod = new QueryExpression();
                                    queryContractMod.EntityName = "fedcap_contractmods";
                                    queryContractMod.ColumnSet = new ColumnSet();
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_name");
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_taskorder");
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_contractmodsid");
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_modificationname");
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_modification");
                                    queryContractMod.ColumnSet.Columns.Add("fedcap_contractmodstype");
                                    queryContractMod.TopCount = 1;
                                    queryContractMod.Criteria = new FilterExpression();
                                    queryTaskOrdersCheck.Criteria.AddCondition("fedcap_modificationname", ConditionOperator.Equal, modificationName);
                                    queryContractMod.Criteria.AddCondition("fedcap_taskorder", ConditionOperator.Equal, new Guid(rfqId));
                                    EntityCollection contractModeList = organizationService.RetrieveMultiple(queryContractMod);
                                    if (contractModeList.Entities.Count > 0)
                                    {
                                        var contractMods = contractModeList.Entities[0];
                                        organizationService.Update(contractMod);
                                        contractMod.Attributes["fedcap_contractmodstype"] = new OptionSetValue(0);
                                        contractMod.Attributes["fedcap_modificationname"] = modificationName;
                                        contractMod.Attributes["fedcap_taskorder"] = new EntityReference(id: new Guid(rfqId), logicalName: "fedcap_taskorder");

                                        contractMod.Id = contractMods.Id;
                                        organizationService.Update(contractMod);
                                    }
                                    Entity TaskOrder = new Entity("fedcap_taskorder");
                                    if (oldTaskOrder.Attributes.Contains("fedcap_is_cancelled"))
                                    {
                                        bool fedcap_is_cancelled = Convert.ToBoolean(oldTaskOrder.Attributes["fedcap_is_cancelled"]);
                                        if (!fedcap_is_cancelled)
                                        {
                                            TaskOrder.Attributes["fedcap_is_cancelled"] = true;
                                        }
                                    }
                                    else
                                    {
                                        TaskOrder.Attributes["fedcap_is_cancelled"] = true;
                                    }
                                    TaskOrder.Id = oldTaskOrder.Id;
                                    organizationService.Update(TaskOrder);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("createContractMods : rfqId " + rfqId + " " + ex.Message.ToString());
            }
        }
    }
}
