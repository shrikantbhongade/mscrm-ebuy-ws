﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace WebServiceMSCRM9._0
{
    public class Helper
    {
        public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void WriteToLogAndConsole(string message)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                log.Info(message);
                Console.WriteLine(message);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
                Console.WriteLine(ex.Message);
            }

        }

        public static void WriteToLogAndConsoleError(Exception ex)
        {
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                string[] splitstring = new string[] { "\r\n" };
                string sTrace = ex.StackTrace.Split(splitstring, System.StringSplitOptions.None)[ex.StackTrace.Split(splitstring, System.StringSplitOptions.None).Count() - 1];
                log.Info(ex.Message + " : " + sTrace);

                Console.WriteLine(ex.Message);
            }
            catch (Exception exception)
            {
                log.Error(exception.Message, exception);
                Console.WriteLine(exception.Message);
            }

        }

        public static void DataTableToCsv(DataTable dataTable, string filePath)
        {

            try
            {
                StringBuilder fileContent = new StringBuilder();

                foreach (var col in dataTable.Columns)
                {
                    fileContent.Append(col.ToString() + ",");
                }

                fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);

                foreach (DataRow dr in dataTable.Rows)
                {

                    foreach (var column in dr.ItemArray)
                    {
                        fileContent.Append("\"" + column.ToString().Replace("\"", "\"\"") + "\",");
                    }

                    fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);

                }
                //fileContent.Replace("\"", System.Environment.NewLine, fileContent.Length - 1, 1);
                //fileContent.AppendFormat("\"{0}\"", fileContent.Replace("\"", ));
                System.IO.File.WriteAllText(filePath, fileContent.ToString());
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsoleError(ex);
            }

        }
    }
}