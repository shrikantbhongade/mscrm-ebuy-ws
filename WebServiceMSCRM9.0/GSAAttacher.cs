﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Text.RegularExpressions;
using System.Web;
using WebServiceMSCRM9._0;
namespace WebServiceMSCRM9._0
{
    public class GSAAttacher
    {
        public static string attachDocTO(string inputBody, string fileName, string taskOrderId)
        {
            try
            {


                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        string fileNameWithoutExtension = fileName.Substring(0, fileName.IndexOf(".")).Trim();
                        fileNameWithoutExtension = Regex.Replace(fileNameWithoutExtension, "[;/:*?]", string.Empty);
                        fileName = Regex.Replace(fileName, "[;/:*?]", string.Empty);
                        QueryExpression records = new QueryExpression("annotation");
                        if (fileNameWithoutExtension != null && fileNameWithoutExtension.Length > 0)
                        {
                            records.ColumnSet = new ColumnSet("subject", "filename", "notetext", "documentbody");

                            FilterExpression Filter1 = new FilterExpression(LogicalOperator.And);
                            Filter1.AddCondition("objectid", ConditionOperator.Equal, new Guid(taskOrderId));

                            FilterExpression Filter2 = new FilterExpression(LogicalOperator.Or);
                            Filter2.AddCondition("filename", ConditionOperator.Equal, fileName.Trim());
                            Filter2.AddCondition("filename", ConditionOperator.Equal, fileNameWithoutExtension.Trim());

                            FilterExpression mainFilter = new FilterExpression(LogicalOperator.And);
                            mainFilter.AddFilter(Filter1);
                            mainFilter.AddFilter(Filter2);
                            records.Criteria = mainFilter;
                            EntityCollection queryEntityRecords = organizationService.RetrieveMultiple(records);

                        }
                        else
                        {
                            records.ColumnSet = new ColumnSet("subject", "filename", "notetext", "documentbody");

                            FilterExpression Filter1 = new FilterExpression(LogicalOperator.And);
                            Filter1.AddCondition("objectid", ConditionOperator.Equal, new Guid(taskOrderId));

                            FilterExpression Filter2 = new FilterExpression(LogicalOperator.And);
                            Filter2.AddCondition("filename", ConditionOperator.Equal, fileName.Trim());

                            FilterExpression mainFilter = new FilterExpression(LogicalOperator.And);
                            mainFilter.AddFilter(Filter1);
                            mainFilter.AddFilter(Filter2);
                            records.Criteria = mainFilter;
                            EntityCollection queryEntityRecords = organizationService.RetrieveMultiple(records);
                        }



                        //Retrieve all notes in entitycollection
                        EntityCollection notes = organizationService.RetrieveMultiple(records);
                        
                        //If notes records found
                        if (notes.Entities.Count > 0)
                        {
                            //Loop through the entitycollection
                            foreach (Entity note in notes.Entities)
                            {
                                // Delete the note record 
                                organizationService.Delete("annotation", note.Id);
                            }
                        }
                    }
                }

                Entity attach = new Entity("annotation");
                //attach["objectid"] = taskOrderId;
                attach["objectid"] = new EntityReference(id: new Guid(taskOrderId), logicalName: "fedcap_taskorder");

                //attach["subject"] = fileName.Trim();
                attach["filename"] = fileName.Trim();
                //attach.ContentType = 'application/pdf';
                attach["documentbody"] = inputBody;
                try
                {
                    organizationService.Create(attach);
                    //insert attach ;
                    return "success - " + fileName;
                }
                catch (Exception ex)
                {
                    Helper.WriteToLogAndConsole("attachDocTO Saving Notes - taskOrderId:-" + taskOrderId + " fileName:-  " + fileName + "; " + ex.Message.ToString());
                    return ex.Message.ToString();
                    //return "failed";
                }
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("attachDocTO in The Method - taskOrderId:-" + taskOrderId + " fileName:-  " + fileName + "; " + ex.Message.ToString());
                return ex.Message.ToString();
            }
        }
    }
}