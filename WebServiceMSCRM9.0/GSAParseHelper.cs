﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Web;
using TimeZoneConverter;

namespace WebServiceMSCRM9._0
{
    public class GSAParseHelper
    {
        public static void InsertContractMods(string taskOrderId,string rfqId,string rfqTitle,string rFQIssueDate,string delivery
                                              ,string trimmedDescription,string reference,string category,string categorybackend
                                              ,string taskOrderContractingActivity,string individualReceivingShipment
                                              ,string rfqIdOld, string rfqTitleOld, string rFQIssueDateOld, string deliveryOld
                                              , string trimmedDescriptionOld, string referenceOld, string categoryOld, string categorybackendOld
                                              , string taskOrderContractingActivityOld, string individualReceivingShipmentOld
                                              ,string buyerId,string buyerIdOld,string accountId,string accountIdOld, string contractingAgency, string contractingAgencyOld
                                              , string rFQCloseDate,string rFQCloseDateOld
            )
        {
            try
            {
                IOrganizationService organizationService = null;
                string CRMServerURL = ConfigurationManager.AppSettings["OrganizationURL"].ToString();
                string userName = ConfigurationManager.AppSettings["Username"].ToString();
                string password = ConfigurationManager.AppSettings["Password"].ToString();
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                organizationService = (IOrganizationService)new OrganizationServiceProxy(new Uri(CRMServerURL),
                 null, clientCredentials, null);
                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    if (userid != Guid.Empty)
                    {
                        var contractMod = new Entity("fedcap_contractmods");
                        contractMod.Attributes["fedcap_contractmodstype"] = new OptionSetValue(1);
                        contractMod.Attributes["fedcap_taskorder"] = new EntityReference(id: new Guid(taskOrderId), logicalName: "fedcap_taskorder");
                        contractMod.Attributes["fedcap_newtaskordernumber"] = rfqId;
                        contractMod.Attributes["fedcap_taskordernumber"] = rfqIdOld;
                        contractMod.Attributes["fedcap_newcategory"] = category.Trim();
                        contractMod.Attributes["fedcap_category"] = categoryOld.Trim();
                        contractMod.Attributes["fedcap_newcategorybackend"] = categorybackend.Trim();
                        contractMod.Attributes["fedcap_categorybackend"] = categorybackendOld.Trim();
                        contractMod.Attributes["fedcap_newtaskordertitle"] = rfqTitle;
                        contractMod.Attributes["fedcap_taskordertitle"] = rfqTitleOld;

                        if (!string.IsNullOrEmpty(buyerId))
                        {
                            try
                            {
                                contractMod.Attributes["fedcap_newbuyer"] = new EntityReference(id: new Guid(buyerId), logicalName: "contact");
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            contractMod.Attributes["fedcap_newbuyer"] = null;
                        }
                        if (!string.IsNullOrEmpty(buyerIdOld))
                        {
                            try
                            {
                                contractMod.Attributes["fedcap_buyer"] = new EntityReference(id: new Guid(buyerIdOld), logicalName: "contact");
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            contractMod.Attributes["fedcap_buyer"] = null;
                        }
                        //***********************start rFQIssueDate******************************************
                        if (!string.IsNullOrEmpty(rFQIssueDate))//&& !string.IsNullOrEmpty(rFQIssueDateOld)
                            {
                            //DateTime dtrFQIssueDate = Convert.ToDateTime(rFQIssueDate);
                            //DateTime dtrFQIssueDateOld = Convert.ToDateTime(rFQIssueDateOld);
                            //contractMod.Attributes["fedcap_newproposalduedate"] = dtrFQIssueDate;
                            //contractMod.Attributes["fedcap_proposalduedate"] = dtrFQIssueDateOld;
                            //contractMod.Attributes["fedcap_newrfptoreleasedate"] = dtrFQIssueDate;
                            //contractMod.Attributes["fedcap_rfptoreleasedate"] = dtrFQIssueDateOld;

                            try
                            {
                                string dateValue = rFQIssueDate;
                                string[] split = dateValue.Split(' ');
                                string strShortTime = (split[split.Length - 1]);
                                string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                dateValue = dateValue.Remove(dateValue.Length - 3);
                                string strTime = tzi.BaseUtcOffset.ToString();
                                dateValue = dateValue + strTime;
                                dateValue = dateValue.Remove(dateValue.Length - 3);
                                string value = dateValue;
                                string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                DateTime dtrFQIssueDate;
                                if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQIssueDate))
                                {
                                    //HttpContext.Current.Response.Write("Converted Existed Date  contractMod " + value + " to " + dtrFQIssueDate + ", Kind " + dtrFQIssueDate.Kind);
                                    if (!string.IsNullOrEmpty(rFQIssueDateOld))
                                    {
                                        DateTime dtrFQIssueDateOld = Convert.ToDateTime(rFQIssueDateOld);
                                        //DateTime dtrFQIssueDateOld = new DateTime(rFQIssueDateOld, DateTimeKind.Utc);// Convert.ToDateTime(rFQIssueDateOld, DateTimeKind.Utc);
                                        //dtrFQIssueDateOld.ToUniversalTime();
                                        //contractMod.Attributes["fedcap_proposalduedate"] = (dtrFQIssueDateOld);
                                        contractMod.Attributes["fedcap_rfptoreleasedate"] = (dtrFQIssueDateOld);
                                    }
                                    string[] splitInt = split[0].Split('/');
                                    DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));
                                    //contractMod.Attributes["fedcap_newproposalduedate"] = dt;// (dtrFQIssueDate);
                                    contractMod.Attributes["fedcap_newrfptoreleasedate"] = dt; //(dtrFQIssueDate);
                                }
                                else
                                {
                                    Helper.WriteToLogAndConsole("InsertContractMods Unable to convert date: - " + value);
                                }
                            }
                            catch (Exception ex)
                            {
                                Helper.WriteToLogAndConsole("InsertContractMods " + ex.Message.ToString());
                            }

                        }
                        else
                        {
                            //contractMod.Attributes["fedcap_newproposalduedate"] = null;
                            //contractMod.Attributes["fedcap_proposalduedate"] = null;
                            contractMod.Attributes["fedcap_newrfptoreleasedate"] = null;
                            contractMod.Attributes["fedcap_rfptoreleasedate"] = null;
                        }
                        //*************end rFQIssueDate****************************************************
                        //***********************start rFQCloseDate******************************************
                        if (!string.IsNullOrEmpty(rFQCloseDate))//&& !string.IsNullOrEmpty(rFQIssueDateOld)
                        {
                            try
                            {
                                string dateValue = rFQCloseDate;
                                string[] split = dateValue.Split(' ');
                                string strShortTime = (split[split.Length - 1]);
                                string strTimeZone = DateTimeHelper.getShortTimeZoneMap(strShortTime);
                                TimeZoneInfo tzi = TZConvert.GetTimeZoneInfo(strTimeZone);
                                dateValue = dateValue.Remove(dateValue.Length - 3);
                                string strTime = tzi.BaseUtcOffset.ToString();
                                dateValue = dateValue + strTime;
                                dateValue = dateValue.Remove(dateValue.Length - 3);
                                string value = dateValue;
                                string fmt = @"MM/dd/yyyy h:mm:ss tt zzz";
                                DateTime dtrFQCloseDate;
                                if (DateTime.TryParseExact(value, fmt, null, DateTimeStyles.AllowWhiteSpaces, out dtrFQCloseDate))
                                {
                                    if (!string.IsNullOrEmpty(rFQCloseDateOld))
                                    {
                                        DateTime dtrFQCloseDateOld = Convert.ToDateTime(rFQCloseDateOld);
                                        contractMod.Attributes["fedcap_proposalduedate"] = (dtrFQCloseDateOld);
                                    }
                                    string[] splitInt = split[0].Split('/');
                                    DateTime dt = new DateTime(Convert.ToInt32(splitInt[2]), Convert.ToInt32(splitInt[0]), Convert.ToInt32(splitInt[1]));
                                    contractMod.Attributes["fedcap_newproposalduedate"] = dt;
                                }
                                else
                                {
                                    Helper.WriteToLogAndConsole("InsertContractMods Unable to convert date: - " + value);
                                }
                            }
                            catch (Exception ex)
                            {
                                Helper.WriteToLogAndConsole("InsertContractMods " + ex.Message.ToString());
                            }

                        }
                        else
                        {
                            contractMod.Attributes["fedcap_newproposalduedate"] = null;
                            contractMod.Attributes["fedcap_proposalduedate"] = null;
                            //contractMod.Attributes["fedcap_newrfptoreleasedate"] = null;
                            //contractMod.Attributes["fedcap_rfptoreleasedate"] = null;
                        }
                        //*************end fedcap_proposalduedate****************************************************
                        contractMod.Attributes["fedcap_newdescription"] = trimmedDescription;
                        contractMod.Attributes["fedcap_description"] = trimmedDescriptionOld;
                        contractMod.Attributes["fedcap_newdelivery"] = delivery;
                        contractMod.Attributes["fedcap_delivery"] = deliveryOld;
                        contractMod.Attributes["fedcap_newreference"] = reference;
                        contractMod.Attributes["fedcap_reference"] = referenceOld;
                        contractMod.Attributes["fedcap_newcontractingactivity"] = taskOrderContractingActivity;
                        contractMod.Attributes["fedcap_contractingactivity"] = taskOrderContractingActivityOld;
                        contractMod.Attributes["fedcap_newindividualreceivingshipment"] = individualReceivingShipment;
                        contractMod.Attributes["fedcap_individualreceivingshipment"] = individualReceivingShipmentOld;
                        contractMod.Attributes["fedcap_newcontractingagencytxt"] = contractingAgency;
                        contractMod.Attributes["fedcap_contractingagencytxt"] = contractingAgencyOld;
                        organizationService.Create(contractMod);
                    }
                }

            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("InsertContractMods:- taskOrderId - " + taskOrderId + " "+ ex.Message.ToString());
               //HttpContext.Current.Response.Write("Contract Mods :- " + ex.Message.ToString());
            }
        }
    }
}