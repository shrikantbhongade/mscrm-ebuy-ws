﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebServiceMSCRM9._0
{
    /// <summary>
    /// Summary description for portal
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class portal : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void SaveTOMASurey()
        {
            string save = string.Empty;
            string taskOrderId = string.Empty;
            string contactId = string.Empty;
            string htmlBody = string.Empty;
            string tomaquestionnaireid = string.Empty;
            Helper.WriteToLogAndConsole("SaveTOMASurey:- method called");
            try
            {
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                    //htmlBody = Convert.ToBase64String(stream.ToArray());
                }
                string[] param = htmlBody.Split('&');
                taskOrderId = param[0].Split('=')[1];
                contactId = param[1].Split('=')[1];
                tomaquestionnaireid = param[2].Split('=')[1];
                save = TOMAResponseCRUD.SaveTOMASurvey_Azure(taskOrderId, contactId, tomaquestionnaireid);
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurey:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(save);
        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public void SaveTOMASurveyAnswers()
        {
            string save = string.Empty;
            string responseId = string.Empty;
            string questionId = string.Empty;
            string givenAnswer = string.Empty;
            string htmlBody = string.Empty;
            Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers:- method called");
            try
            {
                using (var stream = new MemoryStream())
                {
                    var request = HttpContext.Current.Request;
                    request.InputStream.Seek(0, SeekOrigin.Begin);
                    request.InputStream.CopyTo(stream);
                    htmlBody = Encoding.UTF8.GetString(stream.ToArray());
                    //htmlBody = Convert.ToBase64String(stream.ToArray());
                }
                string[] param = htmlBody.Split('&');
                responseId = param[0].Split('=')[1];
                questionId = param[1].Split('=')[1];
                givenAnswer = param[2].Split('=')[1];
                //save = "responseId:-" + responseId + " questionId:-" + questionId + " givenAnswer:-" + givenAnswer;
                save = TOMAResponseCRUD.SaveTOMASurveyAnswers_Azure(responseId, questionId, givenAnswer);
            }
            catch (Exception ex)
            {
                Helper.WriteToLogAndConsole("SaveTOMASurveyAnswers:- " + ex.Message.ToString());

            }
            HttpContext.Current.Response.Write(save);
        }
    }
}
